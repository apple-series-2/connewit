source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.1'

gem 'rails', '5.2.1'
gem 'bcrypt', '~> 3'
gem 'bootstrap', '~> 4.1'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'puma',         '3.9.1'
gem 'sass-rails',   '5.0.6'
gem 'uglifier',     '3.2.0'
gem 'coffee-rails', '4.2.2'
gem 'turbolinks',   '5.0.1'
gem 'jbuilder',     '2.7.0'
gem 'bootsnap', require: false
gem 'aws-sdk-s3', require: false
gem 'rails-i18n'
gem 'kaminari'
gem 'image_processing', '~> 1.2'
gem 'pg', '0.20.0'
gem 'enum_help'
gem 'mini_magick'
gem 'draper'
gem 'fullcalendar-rails'
gem 'momentjs-rails'
gem 'faker', '1.7.3'
gem 'font-awesome-sass', '~> 5.2.0'
gem 'config'
gem 'react-rails'
gem 'webpacker'
gem 'js-routes'
gem 'mini_racer'

group :development do
  gem 'rubocop-airbnb'
  gem 'web-console',           '3.5.1'
  gem 'listen',                '3.1.5'
  gem 'spring',                '2.0.2'
  gem 'spring-watcher-listen', '2.0.1'
end

group :production, :staging do
    gem 'unicorn'
end

group :test do
  gem 'factory_bot_rails'
  gem 'pry-byebug', '~> 3.9'
  gem 'pry-doc'
  gem 'pry-rails'
  gem 'rails-erd'
  gem 'annotate'
  gem 'capybara'
  gem 'webdrivers'
  gem 'capybara-screenshot', github: 'mattheworiordan/capybara-screenshot'
  gem 'webmock', require: false
  gem 'shoulda-matchers'
end

# Windows環境ではtzinfo-dataというgemを含める必要があります
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
