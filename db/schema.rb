# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_07_13_114608) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "articles", force: :cascade do |t|
    t.string "title", null: false
    t.text "content"
    t.boolean "emergency", default: false, null: false
    t.integer "type", default: 0, null: false
    t.date "start_date"
    t.date "end_date"
    t.bigint "user_id"
    t.bigint "representative_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image"
    t.string "parent_tag"
    t.string "child_tag"
    t.integer "plan_days"
    t.boolean "complete_function", default: false
    t.bigint "member_id"
    t.index ["end_date"], name: "index_articles_on_end_date"
    t.index ["member_id"], name: "index_articles_on_member_id"
    t.index ["representative_id"], name: "index_articles_on_representative_id"
    t.index ["start_date"], name: "index_articles_on_start_date"
    t.index ["type"], name: "index_articles_on_type"
    t.index ["user_id"], name: "index_articles_on_user_id"
  end

  create_table "cities", force: :cascade do |t|
    t.string "name", null: false
    t.integer "weather_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "incompletes", force: :cascade do |t|
    t.bigint "article_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "completed", default: false
    t.index ["article_id"], name: "index_incompletes_on_article_id"
    t.index ["user_id"], name: "index_incompletes_on_user_id"
  end

  create_table "members", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_members_on_user_id"
  end

  create_table "representatives", force: :cascade do |t|
    t.string "name", null: false
    t.string "email", null: false
    t.string "password_digest", null: false
    t.string "activation_digest"
    t.boolean "activated", default: false
    t.datetime "activated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "destroy_digest"
    t.datetime "destroy_sent_at"
    t.index ["email"], name: "index_representatives_on_email", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "name", null: false
    t.string "email"
    t.string "staff_id", null: false
    t.string "password_digest", null: false
    t.boolean "admin", default: false
    t.boolean "sender"
    t.string "context"
    t.string "phone_number"
    t.string "remember_digest"
    t.string "reset_digest"
    t.datetime "reset_sent_at"
    t.bigint "representative_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "city", default: "東京"
    t.integer "city_id", default: 1850147
    t.index ["email", "staff_id", "representative_id"], name: "index_users_on_email_and_staff_id_and_representative_id"
    t.index ["name"], name: "index_users_on_name"
    t.index ["representative_id"], name: "index_users_on_representative_id"
  end

  add_foreign_key "articles", "members"
  add_foreign_key "articles", "representatives"
  add_foreign_key "articles", "users"
  add_foreign_key "incompletes", "articles"
  add_foreign_key "incompletes", "users"
  add_foreign_key "members", "users"
  add_foreign_key "users", "representatives"
end
