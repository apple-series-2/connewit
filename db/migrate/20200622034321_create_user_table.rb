class CreateUserTable < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name, null: false
      t.string :email, unique: true
      t.string :staff_id, null: false
      t.string :password_digest, null: false
      t.boolean :admin, default: false
      t.boolean :sender
      t.string :context
      t.string :phone_number
      t.string :remember_digest
      t.string :reset_digest
      t.datetime :reset_sent_at
      t.integer :representative_id

      t.timestamps

      t.references :representative, foreign_key: true
    end
    add_index :users, [:email, :staff_id, :representative_id]
  end
end
