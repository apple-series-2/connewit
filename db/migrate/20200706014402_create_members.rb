class CreateMembers < ActiveRecord::Migration[5.2]
  def change
    create_table :members do |t|
      t.string :name, null: false
      t.integer :user_id
      t.timestamps

      t.references :user, foreign_key: true
    end
    add_index :users, [:name]
  end
end
