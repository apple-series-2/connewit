class AddDestroyDigestToRepresentative < ActiveRecord::Migration[5.2]
  def change
    add_column :representatives, :destroy_digest, :string
    add_column :representatives, :destroy_sent_at, :datetime
  end
end
