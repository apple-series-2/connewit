class CreateArticleTable < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string :title, null: false
      t.text :content
      t.boolean :emergency, null: false, default: false
      t.string :type, index: true, null: false
      t.date :start_date, index: true
      t.date :end_date, index: true
      t.integer :user_id
      t.integer :representative_id

      t.timestamps
      t.references :user, foreign_key: true
      t.references :representative, foreign_key: true
    end
  end
end
