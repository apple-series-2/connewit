class AddCompletedToIncompletes < ActiveRecord::Migration[5.2]
  def change
    add_column :incompletes, :completed, :boolean, default: false
  end
end
