class AddCompleteFunctionToArticles < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :complete_function, :boolean, default: false
  end
end
