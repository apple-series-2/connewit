class AddPrefectureToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :city, :string, default: '東京'
    add_column :users, :city_id, :integer, default: 1850147
  end
end
