class AddTagToArticles < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :parent_tag, :string
    add_column :articles, :child_tag, :string
  end
end
