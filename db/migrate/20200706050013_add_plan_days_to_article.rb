class AddPlanDaysToArticle < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :plan_days, :integer
  end
end
