class CreateRepresentativeTable < ActiveRecord::Migration[5.2]
  def change
    create_table :representatives do |t|
      t.string :name, null: false
      t.string :email, null: false
      t.string :password_digest, null: false
      t.string :activation_digest
      t.boolean :activated, default: false
      t.datetime :activated_at

      t.timestamps
    end
    add_index :representatives, :email, unique: true
  end
end
