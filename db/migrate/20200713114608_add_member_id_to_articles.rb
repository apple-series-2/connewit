class AddMemberIdToArticles < ActiveRecord::Migration[5.2]
  def change
    add_reference :articles, :member, foreign_key: true
  end
end
