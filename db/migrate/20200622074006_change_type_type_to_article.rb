class ChangeTypeTypeToArticle < ActiveRecord::Migration[5.2]
  def up
    change_column :articles, :type, 'integer USING CAST(type AS integer)', null: false, index: true, default: 0
  end
  
  def down
    change_column :articles, :type, :string, null: false, index: true
  end
end
