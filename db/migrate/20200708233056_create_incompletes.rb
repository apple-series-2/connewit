class CreateIncompletes < ActiveRecord::Migration[5.2]
  def change
    create_table :incompletes do |t|
      t.integer :article_id
      t.integer :user_id

      t.references :article, foreign_key: true
      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end
