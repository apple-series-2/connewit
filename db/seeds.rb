# 代表者アカウント
Representative.create!( name:     'Not Activated',
                        email:    'example@example.com',
                        password: 'password')

2.times do |n|
  name     = "Example #{n}"
  email    = "example-#{n}@example.com"
  password = 'password'
  Representative.create!( name:         name,
                          email:        email,
                          password:     password,
                          activated:    true,
                          activated_at: Time.zone.now )
end

# ユーザー
representatives = Representative.where(activated: true)
representatives.each do |rep|
  # admin
  name         = Faker::Name.name
  password     = 'password'
  context      = '人事部'
  phone_number = Faker::PhoneNumber.phone_number.delete("-")
  email        = "#{rep.id}-admin@example.com"
  staff_id     = "#{rep.id}Admin"

  rep.users.create!( name:         name,
                     email:        email,
                     staff_id:     staff_id,
                     admin:        true,
                     password:     password,
                     sender:       'sender',
                     context:      context,
                     phone_number: phone_number )
  # sender
  2.times do |n|
    name         = Faker::Name.name
    password     = 'password'
    contexts      = ['店舗運営部', '人事部']
    phone_number = Faker::PhoneNumber.phone_number.delete("-")
    email        = "#{rep.id}-sender#{n}@example.com"
    staff_id     = "#{rep.id}Sender#{n}"

    rep.users.create!( name:         name,
                       email:        email,
                       staff_id:     staff_id,
                       password:     password,
                       sender:       'sender',
                       context:      contexts[n],
                       phone_number: phone_number)
  end

  # receiver
  4.times do |n|
    names         = ['Conne新宿店', 'Conne渋谷店', 'Conne東京駅前店', 'Conne秋葉原店']
    password     = 'password'
    context      = '直営店舗'
    phone_number = Faker::PhoneNumber.phone_number.delete("-")
    email        = "#{rep.id}-receiver#{n}@example.com"
    staff_id     = "#{rep.id}Receiver#{n}"

    rep.users.create!( name:         names[n],
                       email:        email,
                       staff_id:     staff_id,
                       password:     password,
                       sender:       'receiver',
                       context:      context,
                       phone_number: phone_number)
  end
end

# 情報・予定・メンバー
representatives.each do |rep|
  senders = rep.sender_users
  receivers = rep.receivers
  users = rep.users

  senders.sample(2).each do |sender|
    # 情報
    1.times do
      title = Faker::Lorem.word
      content = Faker::Lorem.sentence(15)
      
      sender.articles.create!( title:             '今週の新商品',
                               content:           content,
                               type:              'information',
                               start_date:        Date.today.next_day(2),
                               end_date:          Date.today.next_day(5),
                               parent_tag:        '商品情報',
                               child_tag:         '新商品',
                               representative_id: rep.id )

      sender.articles.create!( title:            title + '使用方法',
                               content:           content,
                               type:              'information',
                               start_date:        Date.today.next_day(2),
                               end_date:          Date.today.next_day(3),
                               parent_tag:        '使用方法',
                               child_tag:         'ツール',
                               representative_id: rep.id )
    end

    # 緊急情報
    1.times do |n|
      title = Faker::Lorem.word
      content = Faker::Lorem.sentence(5)
      
      sender.articles.create!( title:             '価格の誤表記発覚',
                               content:           content,
                               emergency:         true,
                               type:              'information',
                               start_date:        Date.today.next_day(0),
                               end_date:          Date.today.next_day(2),
                               parent_tag:        '緊急情報',
                               child_tag:         '緊急施策',
                               representative_id: rep.id )
    end
  end

  # 予定
  users.sample(2).each do |user|
    2.times do |n|
      titles  = ['店長会', 'ミーティング']
      content = Faker::Lorem.sentence(5)
      
      user.articles.create!( title:             titles[n],
                             content:           content,
                             type:              'plans',
                             start_date:        Date.today.next_day(3),
                             representative_id: rep.id )
    end
  end

  receivers.sample(3).each do |receiver|
    2.times do |n|
      titles  = ['ミーティング', 'VMD変更', '商品在庫整理', 'アンケート回答', '上長へ売上報告', '店舗施策'].sample(2)
      dates   = [0,1,2,3,4,5,6,7,8,9]
      start_date = dates.sample(1)
      end_date   = dates.select { |x| x >= start_date[0] }.sample(1)
      content = Faker::Lorem.sentence(10)
      
      receiver.articles.create!( title:             titles[n],
                                 content:           content,
                                 type:              'plans',
                                 start_date:        Date.today.next_day(start_date[0]),
                                 end_date:          Date.today.next_day(end_date[0]),
                                 representative_id: rep.id )
    end
  end

  # images
  rep.articles.sample(3).each do |article|
    article.images.attach(io: File.open(Rails.root + 'public/samplejpg1.jpg'), filename: 'samplejpg1.jpg')
  end

  # members
  receivers.sample(3).each do |receiver|
    2.times do
      receiver.members.create!( name: Faker::Name.name )
    end
  end
end

# City
cities = [
  ['旭川', 2130629],
  ['札幌', 2128295],
  ['釧路', 2129376],
  ['青森', 2130658],
  ['秋田', 2113124],
  ['盛岡', 2111834],
  ['山形', 2110556],
  ['仙台', 2111149],
  ['福島', 2112923],
  ['いわき', 2112539],
  ['新潟', 1855431],
  ['長野', 1856215],
  ['金沢', 1860243],
  ['宇都宮', 1849053],
  ['東京', 1850147],
  ['千葉', 2113015],
  ['横浜', 2127436],
  ['高山', 1850892],
  ['岐阜', 1863641],
  ['静岡', 1851715],
  ['名古屋', 1856057],
  ['舞鶴', 1857766],
  ['豊岡', 1849831],
  ['京都', 1857910],
  ['神戸', 1859171],
  ['大阪', 1853909],
  ['津', 1849796],
  ['奈良', 1855612],
  ['和歌山', 1926004],
  ['鳥取', 1849892],
  ['岡山', 1854383],
  ['広島', 1862415],
  ['山口', 1848689],
  ['高松', 1851100],
  ['徳島', 1850158],
  ['松山', 1926099],
  ['高知', 1859146],
  ['福岡', 1863967],
  ['佐賀', 1853303],
  ['大分', 1854487],
  ['熊本', 1858419],
  ['長崎', 1856177],
  ['宮崎', 1856710],
  ['鹿児島', 1860827],
  ['那覇', 1856035]
]

cities.each do |city|
  City.create!(
    name:       city[0],
    weather_id: city[1]
  )
end
