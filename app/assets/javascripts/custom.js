// Representative
// show
$(document).on('turbolinks:load', function() {
  // 'もっと見る'ボタンの連打を防ぐ
  $('.more a').click(function(e){
    $(this).css('pointer-events', 'none');
  });
});


// User
// form
$(document).on('turbolinks:load', function() {
  $('.form-group #user_sender').change(function() {
    var sender = $('.form-group #user_sender option:selected').val();
    // senderがsenderの場合email入力必須のコメントを出す
    if (sender == 'sender') {
      $('label[for="user_email"]').addClass('required-email');
    } else {
      $('label[for="user_email"]').removeClass('required-email');
    }
  });

  //changeイベントはinput要素が変更を完了した時に実行
  $('.staff_id-form').change(function(){
    //要素のvalue属性を変数に代入。
    var text = $(this).val();
    //全角英数字を対象に置き換え
    var normalizedText = text.replace(/[！-～]/g,function(s){
      return String.fromCharCode(s.charCodeAt(0)-0xFEE0);
    });
    normalizedText = jQuery.trim(normalizedText)
        .replace(/ /g,"")
        .replace(/　/g,"")
        .replace(/￥/g,"\\")
        .replace(/〜/g,"~")
        .replace(/’/g,"'")
        .replace(/”/g,"\"");
    //要素のvalue属性に変換した変換後の文字を入れる。
    $(this).val(normalizedText);
  });
});

// Article
// tag_search
$(document).on('turbolinks:load', function() {
  jQuery(document).ready(function(){
    $('.child_tag-form').autocomplete({
      minLength: 0,
      source: function(request, response) {
        $.ajax({
          type: 'GET',
          url: '/tag_suggest',
          data: {
            keyword: request.term,
            main_tag: $('.parent_tag-form').val()
          },
          dataType: "json"
        }).then(
          function(data) {
            response(data);
          },
          function(xhr, ts, err){
            alert(xhr.status + "ERROR：検索候補データの取得時にエラーが発生しました。");
            $('.child_tag-form').off();
          }
        );
      },
      autoFocus: true
    });

    // フォームクリックで候補を表示する
    $(".child_tag-form").focusin(function(){
      $(this).autocomplete("search","");
    });

    // メインタグが変更された場合サブタグをクリア
    $(document).on('change keyup','.parent_tag-form', function(){
      $('.child_tag-form').val('');
      // メインタグが未入力の場合サブタブを編集できなくする
      if ($('.parent_tag-form').val() == "" ) {
        $('.child_tag-form').prop("disabled", true);
        $('.child_tag-form').val('');
      } else {
        $('.child_tag-form').prop("disabled", false);
      }
    });

    $(document).on('change', '')
  });
});

// ロード時メインタグが入力済みの場合サブタグを入力できるようにする
$(document).on('turbolinks:load', function() {
  // タイミングを少し遅らせる
  setTimeout(function(){
    if ($('.parent_tag-form').val() == "" ) {
      $('.child_tag-form').prop("disabled", true);
      $('.child_tag-form').val('');
    } else {
      $('.child_tag-form').prop("disabled", false);
    }
  },1000);
});

$(document).on('turbolinks:load', function() {
  jQuery(document).ready(function(){
    $(document).on('change','.article-order', function() {
      $('#order-submit').submit();
    });
  });
});

// article_form
// 添付ファイルが選択されたらtype=fileフォームを追加する
jQuery(document).ready(function(){
  $(document).on('change','.form-control-file',function() {
    if($(this).val()) {
      var clonecode = $('.form-control-file:last').clone(true);
      clonecode.val('');
      clonecode.addClass('add_form-control-file')
      clonecode.insertAfter($('.form-control-file:last'));
    } else {
      $('.add_form-control-file').each(function() {
        if($(this).val() == ""){
          $(this).remove();
          return false;
        }
      });
    }
  });
});

// 情報発信のみ作業完了機能チェックボタンを出現させる
// ロード時
$(document).on('turbolinks:load', function() {
  if($('#article_type').val() == 'plans') {
    setTimeout(function(){
      $('.child_tag-form').prop("disabled", true);
      $('.child_tag-form').val('');
      $('.parent_tag-form').prop("disabled", true);
      $('.parent_tag-form').val('');
    },600);
    $('#complete_check-wrapper').html(
      '<input value="1" type="hidden" name="article[complete_function]" id="article_complete_function">'
    );
  }
});

// 値変更時
$(document).on('change','#article_type', function(){
  if($('#article_type').val() == 'plans') {
    $('.child_tag-form').prop("disabled", true);
    $('.child_tag-form').val('');
    $('.parent_tag-form').prop("disabled", true);
    $('.parent_tag-form').val('');
    $('#complete_check-wrapper').html(
      '<p>※予定の登録ではタグ登録、作業完了確認にチェックは行なえません。</p>' +
      '<input value="1" type="hidden" name="article[complete_function]" id="article_complete_function">'
    );
  } else {
    $('.parent_tag-form').prop("disabled", false);
    $('#complete_check-wrapper').append(
      '<input name="article[complete_function]" type="hidden" value="0"></input>' +
      '<label for="article_complete_function">作業の完了を確認できるようにする</label>' +
      '<input class="form-check-input" type="checkbox" value="0" name="article[complete_function]" id="article_complete_function"></input>' +
      '<p>この項目にチェックをすると、発信された作業内容を完了させた受信側のユーザーを確認することが出来ます。</p>'
    );
  }
});

// メンバー編集エリア表示
$(document).on('turbolinks:load', function() {
  $(function () {
    $(document).on("click", ".js-edit-member-button", function () {             // # ①クリックイベント
      const memberId = $(this).data('member-id');                   // # ②一意のmember.idを代入
      const memberLabelArea = $('#js-member-label-' + memberId);   // # ③一意のラベルを代入
      const memberTextArea = $('#js-textarea-member-' + memberId); // # ④一意のメンバーエリアを代入
      const memberButton = $('#js-member-button-' + memberId);     // # ⑤一意のボタンエリアを代入

      memberLabelArea.hide(); // ③を非表示
      memberTextArea.show();  // ④を表示
      memberButton.show();    // ⑤を表示
    });
  });

  // メンバー名編集エリア非表示
  $(function () {
    $(document).on("click", ".member-cancel-button", function () {
      const memberId = $(this).data('cancel-id');
      const memberLabelArea = $('#js-member-label-' + memberId);
      const memberTextArea = $('#js-textarea-member-' + memberId);
      const memberButton = $('#js-member-button-' + memberId);
      const memberError = $('#js-member-post-error-' + memberId);

      memberLabelArea.show();
      memberTextArea.hide();
      memberButton.hide();
      memberError.hide();
    });
  });

  // メンバー名更新ボタン
  $(function () {
    $(document).on("click", ".member-update-button", function () {
      const memberId = $(this).data('update-id');
      const repId = $(this).data('rep-id');
      const textField = $('#js-textarea-member-' + memberId); //# ①
      const name = textField.val();                           //# ②textFieldの内容を取得
      console.log(name);

      $.ajax({                         //# ③ajax関数
        url: '/staff/representatives/' + repId + '/members/' + memberId, //# ④members/updateアクションにリクエスト
        type: 'PATCH',                 //# ⑤HTTP通信の種類： updateアクションなので'PATCH'と記述
        data: {                        //# ⑥memberモデルのnameカラムに変数nameを格納
          member: {
            name: name
          }
        }
      }).done(function (data) { // ①ajax通信が成功した時の処理
        const memberLabelArea = $('#js-member-label-' + memberId);
        const memberTextArea = $('#js-textarea-member-' + memberId);
        const memberButton = $('#js-member-button-' + memberId);
        const successMessage = $('#js-member-post-success-' + memberId);
        const memberError = $('#js-member-post-error-' + memberId); // ②

        memberLabelArea.show();
        memberLabelArea.text(data.name); // ③データベースにupdateされたbodyのvalueが欲しい
        memberTextArea.hide();
        memberButton.hide();
        memberError.hide();
        successMessage.text('名前を変更しました。');
        $(successMessage).slideDown("slow", function () {
          $(this).delay(3000).slideUp("slow");
        });
      }).fail(function () {
        const memberError = $('#js-member-post-error-' + memberId); // ①コメントエラー用の空タグ
        memberError.text('名前を入力してください');               // ②空タグにメッセージを書き換える
        $('#js-member-post-error-' + memberId).slideDown("normal", function () {
          $(this).delay(5000).slideUp("normal");
        });
      })
    });
  });
});

// header
$(document).on('turbolinks:load', function() {
  var navHeight = $('.rp-header').outerHeight();
  var winWidth = window.innerWidth ? window.innerWidth: $(window).width();
  var minWidth = '768'
  var navPos = '43'

  $(window).resize(function() {
    var winWidth = window.innerWidth ? window.innerWidth: $(window).width();
    if ( winWidth < minWidth ) {
      $('.fixed-bland').attr('id', 'header-small-logo');
      $('.emptiness-a').css('display', 'block');
      $('.rp-header').addClass('header-is-fixed');
      $('.header-nav_bottom .nav-link').addClass('toggle-nav-padding');
    } else {
      var value = $(this).scrollTop();
      $('.rp-header').removeClass('header-is-fixed');
      $('.header-nav_bottom .nav-link').removeClass('toggle-nav-padding');
      if ( value > navPos ) {
        $('.rp-header').addClass('is-fixed');
        $('.fixed-bland').attr('id', 'header-small-logo');
        $('.wrapper').css('margin-top', navHeight)
      } else {
        $('.rp-header').removeClass('is-fixed');
        $('.fixed-bland').removeAttr('id', 'header-small-logo');
        $('.wrapper').css('margin-top', 0)
      }
    }
  });

  $(window).on('load', function() {
    if ( winWidth < minWidth ) {
      $('.fixed-bland').attr('id', 'header-small-logo');
      $('.emptiness-a').css('display', 'block');
      $('.rp-header').addClass('header-is-fixed');
      $('.header-nav_bottom .nav-link').addClass('toggle-nav-padding');
    } else {
      $('.header-logo').removeAttr('id', 'header-small-logo');
      $('.rp-header').removeClass('header-is-fixed');
      $('.header-nav_bottom .nav-link').removeClass('toggle-nav-padding');
    }
  }); 

  $(window).on('load scroll resize', function() {
    var winWidth = window.innerWidth ? window.innerWidth: $(window).width();
    var value = $(this).scrollTop();
    
    if ( winWidth > minWidth ) {
      $('.rp-header').removeClass('header-is-fixed');
      $('.header-nav_bottom .nav-link').removeClass('toggle-nav-padding');
      if ( value > navPos ) {
        $('.rp-header').addClass('is-fixed');
        $('.fixed-bland').attr('id', 'header-small-logo');
        $('.wrapper').css('margin-top', navHeight)
      } else {
        $('.rp-header').removeClass('is-fixed');
        $('.fixed-bland').removeAttr('id', 'header-small-logo');
        $('.wrapper').css('margin-top', 0)
      }
    } else {
      $('.rp-header').addClass('header-is-fixed');
      $('.header-nav_bottom .nav-link').addClass('toggle-nav-padding');
    }
  });
});


// static_pages
// home
// rp_main-visualの高さを画面いっぱいにする
$(document).on('turbolinks:load', function() {
  var header_h = $('.header').outerHeight();
  var window_h = window.innerHeight ? window.innerHeight: $(window).height();
  $('.rp_main-visual').outerHeight(window_h - header_h);
});

// コンテンツをスクロールしたら表示
$(document).on('turbolinks:load', function() {
  $(window).on('load', function() {
    $('.main-visual_image img').addClass('magictime spaceInDown')
  });
  $(window).on('scroll', function() {
    $('.rp_image img').each(function(){
      $(this).css('visibility','hidden');
      var imgPos = $(this).offset().top;
      var scroll = $(window).scrollTop();
      var windowHeight = $(window).height();
      if (scroll > (imgPos - windowHeight - windowHeight/5)) {
        $(this).addClass('magictime spaceInDown')
        $(this).css('visibility', 'visible')
      }
    });
  });
});


// スライドダウンマークを表示、スクロールでTOPボタンに変更
$(document).on('turbolinks:load', function() {
  $(function () {
    setInterval(arrow_down, 5000);
  });

  var spaceInUp = function() {
    $('.arrow-down').removeClass('magictime spaceOutDown');
    $('.arrow-down').addClass('magictime spaceInUp');
  }
  var arrow_down = function() {
    $('.arrow-down').removeClass('magictime spaceOutDown spaceInUp');
    $('.arrow-down').addClass('magictime spaceOutDown');
    setTimeout(spaceInUp, 1000);
  }

  $(window).scroll(function () {
    if ($(this).scrollTop() > 150) {
      $('.arrow-down').toggleClass('arrow-up')
      .toggleClass('arrow-down');
      $('.arrow-up').addClass('animate__animated animate__bounce')
    } else {
      $('.arrow-up').toggleClass('arrow-up')
      .toggleClass('arrow-down');
      $('.arrow-down').removeClass('animate__animated animate__bounce')
    }
  });

  $(document).on('click','.arrow-up', function(){
    $('body,html').animate({
      scrollTop: 0
    }, 250);
  });
});

// gray-wrapper-headlineの高さを設定
$(document).on('turbolinks:load', function() {
  var headline_h = $('.rp_content_headline').outerHeight();
  $('.gray-wrapper_headline').css('height',headline_h);
  $(window).resize(function() {
    var headline_h = $('.rp_content_headline').outerHeight();
    $('.gray-wrapper_headline').css('height',headline_h);
  });
});

// flash
$(document).on('turbolinks:load', function() {
  $('#flash_message').slideDown("slow", function () {
    $(this).delay(5000).slideUp("slow");
  });
});
