class UserMailer < ApplicationMailer
  def account_activation(representative)
    @rep = representative
    mail to: representative.email, subject: '【ConneWat】 会員仮登録完了のお知らせ'
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: '【ConneWat】 パスワードの再設定'
  end

  def destroy_account(user)
    @user = user
    mail to: user.email, subject: '【ConneWat】 アカウントの削除'
  end
  # send a signup email to the user, pass in the user object that   contains the user's email address
  def send_signup_email(user)
    @user = user
    mail( :to => @user.email,
    :subject => 'Thanks for signing up for our amazing app' )
  end
end
