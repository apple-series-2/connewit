class ArticleDecorator < ApplicationDecorator
  delegate_all
  include Rails.application.routes.url_helpers
  include Kaminari::Helpers::UrlHelper

  def all_destroy_link
    h.tag.div(class: 'all_destroy-incomplete') do
      if type == 'information'
        h.link_to '作業完了チェック機能を削除する', representative_article_all_incompletes_path(h.current_representative, self),
                  method: :delete,
                  data: { confirm: "作業完了チェック機能を削除します。よろしいですか？" }
      end
    end
  end

  def article_completed_icon
    c_user = h.current_user
    if c_user.sender? && type == 'information'
      h.tag.p(class: 'completed-list-link') do
        h.link_to '皆の作業状態を見る', representative_article_incompletes_path(h.current_representative, self)
      end
    elsif incomplete(c_user) || c_user.sender? && type == 'plans' && incomplete(user)
      if @incomplete.completed?
        h.tag.p '完了済み', class: 'article_complete-icon article_completed-icon'
      else
        h.tag.p '未完了', class: 'article_incomplete-icon article_completed-icon'
      end
    else
      h.tag.div(class: 'not-complete-function') {}
    end
  end

  def completed_button
    if incomplete(h.current_user)
      if @incomplete.completed?
        h.render 'shared/article_incomplete', incomplete: @incomplete, article: self
      else
        h.render 'shared/article_complete', incomplete: @incomplete, article: self
      end
    end
  end

  # 自身のarticleと渡したuserのincompleteがあれば取得
  def incomplete(user)
    @incomplete = incompletes.find_by(user_id: user.id)
  end

  # 予定にメンバーIDが登録されていたらメンバー名、されていなければユーザー名を返す
  def registered_person
    if member_id.present?
      member.name
    else
      user.name
    end
  end
end
