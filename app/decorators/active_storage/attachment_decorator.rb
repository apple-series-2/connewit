class ActiveStorage::AttachmentDecorator < ApplicationDecorator
  delegate_all
  include Rails.application.routes.url_helpers

  def image_destroy_path(article)
    representative_article_image_path(article.representative, article, self)
  end
end
