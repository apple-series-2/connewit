class Staff::MembersForm
  include ActiveModel::Model

  attr_accessor :collection
  MEMBER_NUM = 5

  # 初期化メソッド
  def initialize(attributes = [])
    if attributes.present?
      self.collection = attributes.map do |value|
        Member.new(
          name: value['name'],
          user_id: value['user_id']
        )
      end
    else
      self.collection = MEMBER_NUM.times.map { Member.new }
    end
  end

  # レコードが存在するか確認するメソッド
  def persisted?
    false
  end

  # コレクションをDBに保存するメソッド
  def save
    is_success = true
    ActiveRecord::Base.transaction do
      collection.each do |result|
        # バリデーションを全てかけたいからsave!ではなくsaveを使用
        if result.name.present?
          is_success = false unless result.save
        end
      end
      # バリデーションエラーがあった時は例外を発生させてロールバックさせる
      raise ActiveRecord::RecordInvalid unless is_success
    end
  rescue
    p 'エラー'
  ensure
    return is_success
  end
end
