json.array!(@articles) do |article|
  json.extract! article, :id, :title
  json.type article.type
  json.start article.start_date
  json.end article.end_date&.+ 1
  json.url staff_representative_article_url(article.representative.id, article.id, format: :html)
end
