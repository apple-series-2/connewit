import React, { Component } from 'react';

class Article extends Component {
  render(){
    return(
      <div className='functions_article-container'>
        <div className='functions_title-wrapper'>
          <h3 className='functions-title'>連絡情報・予定の登録</h3>
          <p>
            連絡情報の登録を行うと全てのユーザーに情報の共有が行なえます。<br></br>
            ユーザー自身の予定を登録することもでき、共有される範囲は情報発信者の場合同じ情報発信者のユーザー内、受信者の場合は当ユーザー内のみです。<br></br>
            連絡情報の共有は情報発信アカウントのみで行え、予定の登録は全てのユーザーで行なえます。
          </p>
        </div>
        <div className='functions_section-wrapper'>
          <h3 className='functions_section-title'>予定の新規登録</h3>
          <section>
            <h4>作業期間の設定</h4>
            <p>
              作業期間の共有を行うと、トップページに今日の予定、開始日が近い予定、
              期限が近い予定としてそれぞれ自動で表示されます。
            </p>
          </section>
          <section>
            <h4>緊急タグ</h4>
            <p>
              緊急タグにチェックを入れると共有先ユーザーのトップページ上部に優先的に表示されます。<br></br>
              その名の通り緊急性の高い情報に対して使用します。
            </p>
          </section>
          <h3 className='functions_section-title'>連絡情報でのみ使用できる機能</h3>
          <section>
            <h4>作業進捗の確認機能</h4>
            <p>
              作業の完了を確認出来るようにするにチェックを入れると、<br></br>
              その連絡情報において作業が完了している受信ユーザーの確認が行えるようになります。<br></br>
              各ページに存在する該当の連絡情報から進捗状況を確認するボタンをクリックすることで確認が行えます。
            </p>
          </section>
          <section>
            <h4>タグ登録機能</h4>
            <p>
              タグを登録するとアーカイブに保存され、タグ情報を元にいつでも検索し閲覧することが出来ます。<br></br>
              （アーカイブページ現在開発中）
            </p>
          </section>
        </div>
      </div>
    );
  }
}

// Infoコンポーネントをexportする。
export default Article;
