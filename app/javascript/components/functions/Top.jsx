import React, { Component } from 'react';

class Top extends Component {
  render(){
    return(
      <div className='functions_top-container'>
        <div className='functions_title-wrapper'>
          <h3 className='functions-title'>トップページ</h3>
          <p>
            トップページでは主に重要度の高い連絡情報が表示されており、<br></br>
            カレンダーや天気予報の確認が行なえます。<br></br>
            作業完了済みの連絡情報は表示されません。
          </p>
        </div>
        <div className='functions_section-wrapper'>
          <section>
            <h4>今日の予定</h4>
            <p>本日の日付が作業期間内である予定が一覧で表示されます。</p>
          </section>
          <section>
            <h4>期限が間近の予定</h4>
            <p>作業の期限日が近い（二日以内）予定が一覧で表示されます。</p>
          </section>
          <section>
            <h4>開始日が間近の予定</h4>
            <p>作業期間が三日間以上予定の中で、作業開始日が近い（二日以内）予定が一覧で表示されます。</p>
          </section>
          <section>
            <h4>緊急配信</h4>
            <p>情報発信者が緊急タグにチェックを付けて配信した連絡情報が存在する場合にのみ表示されます。</p>
          </section>
          <section>
            <h4>天気予報</h4>
            <p>
              ６時間おきの天気予報が表示されます。<br></br>
              地域を変更する場合は天気予報上部にある地域タグから選択、変更することが出来ます。
              </p>
          </section>
          <section>
            <h4>カレンダー</h4>
            <p>
              期間が登録されている連絡情報・予定が表示されます。<br></br>
              表示する範囲は上部ボタンから一ヶ月、一週間、今日の期間で変更できます。
            </p>
          </section>
        </div>
      </div>
    );
  }
}

// Topコンポーネントをexportする。
export default Top;
