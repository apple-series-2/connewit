import React, { Component } from 'react';
// react-router-dom をインポートする
import { HashRouter as Router, NavLink, Switch, Route } from 'react-router-dom';
import Article from './article';
import Top from './Top';
import User from './User';

const FunctionsView = () => (
  // <Router>でAppコンポーネントをラップする
  <Router>
    <div className='functions-wrapper'>
      <NavbarList />
      <div className="animsition">
        <div className='contents-wrapper'>
          <Switch>
            <Route exact path="/" component={ Top }/>
            <Route exact path='/top' component={ Top }/>
            <Route exact path='/user' component={ User }/>
            <Route exact path='/article' component={ Article }/>
          </Switch>
        </div>
      </div>
    </div>
  </Router>
)

const NavbarList = () => (
  <div className='functions_sidebar-wrapper'>
    <div className='functions_sidebar-main'>
      <ul>
        <li className='sidebar_link'>
          <NavLink activeClassName="active" className='animsition-link' to="/top">トップページ</NavLink>
        </li>
        <li className='sidebar_link'>
          <NavLink activeClassName="active" className='animsition-link' to="/user">ユーザー・メンバー</NavLink>
        </li>
        <li className='sidebar_link'>
          <NavLink activeClassName="active" className='animsition-link' to="/article">連絡情報・予定の登録</NavLink>
        </li>
      </ul>
    </div>
  </div>
)

export default FunctionsView
