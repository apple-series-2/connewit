import React from 'react'
// import ReactDOM from 'react-dom'
import FunctionsView from './functions/FunctionsView'

class Functions extends React.Component {
  render () {
    return (
      <React.Fragment>
        <FunctionsView />
      </React.Fragment>
    );
  }
}
// ReactDOM.render(<Functions />, document.getElementById('functions'));

export default Functions
