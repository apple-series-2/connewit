class SessionsController < ApplicationController
  def new
  end

  def create
    if params[:email].present?
      user = User.find_by(email: params[:email].downcase)
    else
      user = User.find_by(staff_id: params[:staff_id])
    end

    if user && user.authenticate(params[:password])
      if user.representative.activated?
        other_sessions_delete
        log_in user
        params[:remember_me] == '1' ? remember(user) : forget(user)
        flash[:success] = 'ログインしました。'
        # before_actionで引っかかって飛ばされた場合はログイン後リクエストされてた場所へ飛ぶようにする
        if params[:path].present?
          redirect_to params[:path]
        else
          if user.sender?
            redirect_to current_representative, "turbolinks" => false
          else
            redirect_to staff_representative_path(current_representative), "turbolinks" => false
          end
        end
      else
        not_activator_redirect
      end
    else
      flash.now[:danger] = 'メールアドレス(ID)もしくはパスワードに誤りがあります。'
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    other_sessions_delete
    flash[:notice] = 'ログアウトしました。'
    redirect_to root_path
  end

  def representative_new
  end

  def representative_create
    rep = Representative.find_by(email: params[:email].downcase)
    if rep && rep.authenticate(params[:password])
      if rep.activated?
        log_in_representative(rep)
        flash[:success] = '代表者情報の認証に成功しました。'
        if session[:params_action] == "new_admin_user"
          redirect_to representative_admin_signup_path(rep)
        else
          redirect_to new_representative_user_path(rep)
        end
      else
        not_activator_redirect
      end
    else
      flash.now[:danger] = 'メールアドレスまたはパスワードに誤りがあります。'
      render 'representative_new'
    end
  end

  private

  def not_activator_redirect
    message = '代表者アカウントが有効化されていません。'
    message += '送信されたメールをチェックし有効化を完了してください。'
    flash[:warning] = message
    redirect_to root_path
  end

  def other_sessions_delete
    session.delete(:params_action)
  end
end
