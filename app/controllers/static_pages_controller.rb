class StaticPagesController < ApplicationController
  def home
    session.delete(:representative_id)
  end

  def functions
    render layout: 'application_functions'
  end
end
