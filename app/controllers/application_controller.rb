class ApplicationController < ActionController::Base
  include SessionsHelper

  def logged_in_user
    if logged_in?
      rep_id = params[:representative_id] || params[:id]
      @representative = Representative.find_by(id: rep_id)
      redirect_to root_path unless current_representative?(@representative)
    else
      if request.method == 'GET'
        redirect_to login_path(path: request.path)
      else
        redirect_to login_path
      end
    end
  end

  def sender_only
    unless current_user.sender?
      flash[:danger] = '情報発信側のユーザーのみアクセスできます。'
      redirect_to root_path
    end
  end

  def logged_in_correct_rep
    session[:params_action] = params[:action]
    if logged_in_rep?
      rep_id = params[:representative_id] || params[:id]
      @representative = Representative.find(rep_id)
      unless current_representative?(@representative)
        flash[:danger] = 'アクセス権限がないサイトへのアクセスです。'
        logged_in? ? redirect_to(@representative) : redirect_to(root_path)
      end
    else
      flash[:info] = '代表者アカウントのメールアドレス、パスワードを入力した後、ユーザー登録画面へと移ります。'
      redirect_to representative_login_path
    end
  end
end
