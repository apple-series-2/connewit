class AccountActivationsController < ApplicationController
  def edit
    rep = Representative.find_by(email: params[:email])
    if !rep.activated?
      if rep && rep.authenticated?(:activation, params[:id])
        rep.activate
        log_in_representative(rep)
        flash[:success] = 'アカウントの有効化に成功しました。最後に管理者ユーザーの登録を行って下さい。'
        redirect_to representative_admin_signup_path(rep)
      else
        flash[:danger] = 'アカウントの有効化に失敗しました。'
        redirect_to root_url
      end
    else
      flash[:notice] = 'このアカウントは既に有効です。'
      redirect_to new_representative_user_path(rep)
    end
  end
end
