class Staff::ArticlesController < Staff::Base
  before_action :correct_article_or_admin, only: [:edit, :update, :destroy]
  before_action :sender_only, only: :create, if: :information_article
  TITLE_LENGTH_MAXIMUM = 50

  layout 'staff_sidebar'

  def index
    art_plans = current_user.plans.page(params[:page]).includes_user_incompletes
    art_information = current_representative.information.page(params[:page]).includes_user_incompletes
    keyword = params[:keyword]
    search_type = params[:type]

    if keyword.present?
      if search_type == '連絡情報'
        @articles = art_information.search(keyword).article_order(params[:order])
      else
        @articles = art_plans.search(keyword).article_order(params[:order])
      end
    else
      if search_type == 'スタッフの予定'
        @articles = art_plans.article_order(params[:order])
      elsif search_type == '未完了'
        @articles = current_user.articles.incompleted_list.page(params[:page]).includes_user_incompletes
      else
        @articles = art_information.article_order(params[:order])
      end
    end
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = current_representative.articles.build
    get_members
  end

  def create
    @article = @representative.articles.build(article_params)
    if @article.end_date && @article.start_date
      @article.plan_days = @article.plan_days_value
    end
    if @article.type == 'plans' && @article.save
      flash[:success] = "#{@article.title}を登録しました。"
      incomplete = @article.incompletes.build(user_id: current_user.id)
      flash[:danger] = 'エラーが発生し作業管理チェック機能が追加されませんでした。' unless incomplete.save
      redirect_to [:staff, current_representative, @article]
    else
      get_members
      render 'new'
    end
  end

  def edit
    @article = @article.decorate
    get_members
  end

  def update
    if params[:article][:complete_function] == '1' && @article.update_attributes(article_params)
      unless @article.user.incompletes.plan_incomplete(@article)
        @article.incompletes.build(user_id: current_user.id)
      end
      flash[:success] = "記事(タイトル: #{@article.title.abridgement_strings(TITLE_LENGTH_MAXIMUM)})を更新しました。"
      redirect_to [:staff, current_representative, @article]
    else
      get_members
      render 'edit'
    end
  end

  def destroy
    title = @article.title
    @article.destroy
    flash[:warning] = "「#{title.abridgement_strings(TITLE_LENGTH_MAXIMUM)}」を削除しました。"
    redirect_to staff_representative_articles_path(current_representative)
  end

  private

  def article_params
    params.require(:article).permit(:title, :content, :emergency, :type, :start_date, :end_date,
                                    :user_id, :representative_id, :member_id, :complete_function, images: [])
  end

  def correct_article_or_admin
    @article = Article.find(params[:id])
    unless correct_model_or_admin?(@article.user)
      flash[:danger] = 'アクセス権限がありません。'
      redirect_to [:staff, @article.representative]
    end
  end

  def get_members
    @members = [[current_user.name, ""]]
    current_user.members.map do |member|
      @members << [member.name, member.id]
    end
  end

  # 受け取ったarticleがinformationの時trueを返す
  def information_article
    if @article
      @article.type == 'information'
    else
      params[:article][:type] == 'information'
    end
  end
end
