class Staff::RepresentativesController < Staff::Base
  PER = 4

  def show
    @user = User.find(session[:user_id])
    @cities = City.all

    included_articles = @representative.articles.includes_user_incompletes
    @emergency_articles = included_articles.information.or(included_articles.where(user: current_user)).
      emergencies.distinct
    @today_plans = included_articles.information.or(included_articles.where(user: current_user)).
      today_plans.distinct.
      page(params[:page]).per(PER)
    @plans_nearing_deadline = included_articles.not_short_plans.information.or(included_articles.where(user: current_user)).
      plans_nearing_deadline.distinct.
      page(params[:page]).per(PER)
    @plans_nearing_start = included_articles.information.or(included_articles.where(user: current_user)).
      plans_nearing_start.distinct.
      page(params[:page]).per(PER)
    if request.xhr?
      render("staff/representatives/plans/#{params[:type]}") && return
    else
      render(layout: 'staff_sidebar') && return
    end
  end

  def event
    articles = current_representative.articles.joins(:user)
    @articles = articles.information.or(articles.where(user: current_user))
  end
end
