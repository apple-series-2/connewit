class Staff::MembersController < Staff::Base
  before_action :set_member, only: [:update, :destroy]

  layout 'staff_sidebar'

  def index
    @members = current_user.members
  end

  def new
    @members = Staff::MembersForm.new
    @members_list = current_user.members
  end

  def create
    @members = Staff::MembersForm.new(members_params)
    if @members.save
      flash[:success] = "#{current_user.name}のメンバーを登録しました。"
      redirect_to staff_representative_members_path(current_representative)
    else
      render 'new'
    end
  end

  def update
    @member.update!(member_update_params)
    render json: @member
  end

  def destroy
    @member.destroy ? flash[:success] = 'メンバーを削除しました。' : flash[:danger] = 'メンバーの削除に失敗しました。'
    redirect_to staff_representative_members_path(@representative)
  end

  private

  def members_params
    params.require(:members)
  end

  def set_member
    @member = current_user.members.find(params[:id])
  end

  def member_update_params
    params.require(:member).permit(:name)
  end
end
