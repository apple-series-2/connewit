class Staff::IncompletesController < Staff::Base
  before_action :correct_incomplete, only: [:destroy]

  layout 'staff_sidebar'

  # 作業完了にする
  def update
    @incomplete = Incomplete.find(params[:id])
    @incomplete.update_attribute(:completed, true)
    @article = @incomplete.article
    respond_to do |format|
      format.html { redirect_back(fallback_location: current_representative) }
      format.js
    end
  end

  # 作業未完了にする
  def destroy
    @incomplete.update_attribute(:completed, false)
    @article = @incomplete.article
    respond_to do |format|
      format.html { redirect_back(fallback_location: current_representative) }
      format.js
    end
  end

  private

  def correct_incomplete
    @incomplete = Incomplete.find(params[:id])
    unless @incomplete && @incomplete.user == current_user # rubocop:disable Airbnb/SimpleUnless
      flash[:danger] = '他ユーザーが持つ作業状況ステータスの変更は行えません。'
      redirect_back(fallback_location: current_representative)
    end
  end
end
