class Admin::Base < ApplicationController
  before_action :logged_in_user, :sender_only

  def current_user_admin
    rep_id = params[:representative_id] || params[:id]
    @representative = Representative.find(rep_id)
    unless current_user && current_user.admin? && current_representative?(@representative) # rubocop:disable Airbnb/SimpleUnless
      redirect_to(root_path)
    end
  end
end
