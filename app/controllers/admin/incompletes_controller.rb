class Admin::IncompletesController < Admin::Base
  before_action :correct_article_or_admin, only: [:all_destroy]
  before_action :correct_incomplete,       only: [:destroy]

  TITLE_LENGTH_MAXIMUM = 20

  def index
    @article = Article.find(params[:article_id])
    @incompletes = @article.incompleted_list
    @completes = @article.completed_list

    render layout: 'admin_sidebar'
  end

  # 作業完了にする
  def update
    @incomplete = Incomplete.find(params[:id])
    @incomplete.update_attribute(:completed, true)
    @article = @incomplete.article
    respond_to do |format|
      format.html { redirect_back(fallback_location: current_representative) }
      format.js
    end
  end

  # 作業未完了にする
  def destroy
    @incomplete.update_attribute(:completed, false)
    @article = @incomplete.article
    respond_to do |format|
      format.html { redirect_back(fallback_location: current_representative) }
      format.js
    end
  end

  def all_destroy
    @article.incompletes.destroy_all
    @article.update_attribute(:complete_function, false)
    flash[:success] = "タイトル: #{@article.title.abridgement_strings(TITLE_LENGTH_MAXIMUM)})の作業管理チェック機能を削除しました。"
    redirect_to edit_representative_article_path(current_representative, @article)
  end

  private

  def correct_article_or_admin
    @article = Article.find(params[:article_id])
    unless correct_model_or_admin?(@article.user)
      flash[:danger] = 'アクセス権限がありません。'
      redirect_to @article.representative
    end
  end

  def correct_incomplete
    @incomplete = Incomplete.find(params[:id])
    unless @incomplete && @incomplete.user == current_user # rubocop:disable Airbnb/SimpleUnless
      flash[:danger] = '他ユーザーが持つ作業状況ステータスの変更は行えません。'
      redirect_back(fallback_location: current_representative)
    end
  end
end
