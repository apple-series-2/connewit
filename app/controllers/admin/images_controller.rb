class Admin::ImagesController < Admin::Base
  protect_from_forgery except: :image_destroy
  before_action :correct_article_or_admin, only: :destroy

  def destroy
    image = @article.images.find(params[:id])
    if image.purge
      redirect_to edit_representative_article_path(current_representative, @article)
    end
  end

  private

  def correct_article_or_admin
    @article = Article.find(params[:article_id])
    unless correct_model_or_admin?(@article)
      flash[:danger] = 'アクセス権限がありません。'
      redirect_to @article.representative
    end
  end
end
