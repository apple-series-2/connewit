class Admin::RepresentativesController < Admin::Base
  skip_before_action :logged_in_user, only: [:new, :create, :new_admin_user]
  skip_before_action :sender_only, only: [:new, :create, :new_admin_user, :event]
  before_action :logged_in_correct_rep,  only: [:new_admin_user]
  before_action :zero_admin_users,       only: [:new_admin_user]
  before_action :current_user_admin,     only: [:edit, :destroy]
  ADMIN_USERS_MAXIMUM = 1
  PER = 4

  def index
  end

  def show
    @user = User.find(session[:user_id])
    @cities = City.all

    included_articles       = @representative.articles.includes_user_incompletes
    @emergency_articles     = included_articles.information.or(included_articles.plans.of_senders).
      emergencies.distinct
    @today_plans            = included_articles.information.or(included_articles.plans.of_senders).
      today_plans.distinct.
      page(params[:page]).per(PER)
    @plans_nearing_deadline = included_articles.not_short_plans.information.or(included_articles.plans.of_senders).
      plans_nearing_deadline.distinct.
      page(params[:page]).per(PER)
    @plans_nearing_start    = included_articles.information.or(included_articles.plans.of_senders).
      plans_nearing_start.distinct.
      page(params[:page]).per(PER)
    if request.xhr?
      render("admin/representatives/plans/#{params[:type]}") && return
    else
      render(layout: 'admin_sidebar') && return
    end
  end

  def event
    articles = current_representative.articles.joins(:user)
    @articles = articles.information.or(articles.plans.of_senders)
  end

  def new
    @representative = Representative.new
  end

  def create
    @representative = Representative.new(rep_params)
    if params[:representative][:terms_check] == "0"
      flash.now[:alert] = '利用規約の確認をして下さい。'
      render 'new'
      return
    end
    if @representative.save
      @representative.send_activation_email
      flash[:info] = "アカウント有効化のメールを送信しました。メールを確認し本登録を完了してください。"
      redirect_to root_path
    else
      render 'new'
    end
  end

  def edit
  end

  def destroy
  end

  def new_admin_user
    @user = @representative.users.build
  end

  private

  def rep_params
    params.require(:representative).permit(:name, :email, :password, :password_confirmation)
  end

  def zero_admin_users
    unless current_representative.admin_users.count < ADMIN_USERS_MAXIMUM
      flash[:warning] = "既に管理者ユーザーが作成されています。管理者ユーザーを二人以上にしたい場合は、管理者アカウントでログイン後のユーザー一覧画面にて既存のユーザーへ権限を付与することが出来ます。"
      redirect_to login_path
    end
  end
end
