class Admin::ArticlesController < Admin::Base
  protect_from_forgery except: :image_destroy
  skip_before_action :logged_in_user, :sender_only, only: [:tag_suggest]
  before_action :correct_article_or_admin, only: [:edit, :update, :destroy]
  TITLE_LENGTH_MAXIMUM = 50

  layout 'admin_sidebar'

  def index
    includes_plans = current_representative.plans.includes_user_incompletes
    art_plans = includes_plans.sender_articles.page(params[:page])
    art_information = current_representative.information.page(params[:page]).includes_user_incompletes
    keyword = params[:keyword]
    search_type = params[:type]
    if keyword.present?
      if search_type == '連絡情報'
        @articles = art_information.search(keyword).article_order(params[:order])
      else
        @articles = art_plans.search(keyword).article_order(params[:order])
      end
    else
      if search_type == 'スタッフの予定'
        @articles = art_plans.article_order(params[:order])
      else
        @articles = art_information.article_order(params[:order])
      end
    end
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = current_representative.articles.build
  end

  def create
    @article = @representative.articles.build(article_params)
    if @article.end_date && @article.start_date
      @article.plan_days = @article.plan_days_value
    end

    if @article.validate_tag && @article.save
      # incomplete保存の分岐
      if @article.type == 'plans'
        incomplete = @article.incompletes.build(user_id: current_user.id)
        flash[:danger] = 'エラーが発生し作業管理チェック機能が追加されませんでした。' unless incomplete.save
      elsif @article.type == 'information' && params[:article][:complete_function] == "1"
        save_incompletes(@article)
      end
      flash[:success] = "#{@article.title}を登録しました。"
      redirect_to [current_representative, @article]
    else
      render 'new'
    end
  end

  def edit
    @article = @article.decorate
  end

  def update
    if @article.update_attributes(article_params)
      # incomplete保存の分岐
      if @article.type == 'plans'
        # 既に同じincompleteがある場合はその場で処理終了
        return redirect_to [current_representative, @article] if @article.incompletes.find_by(user_id: current_user.id)
        incomplete = @article.incompletes.build(user_id: current_user.id)
        flash[:danger] = 'エラーが発生し作業管理チェック機能が追加されませんでした。' unless incomplete.save
      elsif @article.type == 'information' && params[:article][:complete_function] == "1"
        save_incompletes(@article) if @article.incompletes.empty?
      end
      flash[:success] = "記事(タイトル: #{@article.title.abridgement_strings(TITLE_LENGTH_MAXIMUM)})を更新しました。"
      redirect_to [current_representative, @article]
    else
      render 'edit'
    end
  end

  def destroy
    title = @article.title
    @article.destroy
    flash[:warning] = "記事(タイトル: #{title.abridgement_strings(TITLE_LENGTH_MAXIMUM)})を削除しました。"
    redirect_to representative_articles_path(current_representative)
  end

  def tag_suggest
    @tags = current_representative.articles.
      child_tags(params[:main_tag]).
      search_child_tags(params[:keyword]).
      child_tags_asc.
      distinct.pluck(:child_tag)
    render json: @tags
  end

  private

  def article_params
    params.require(:article).permit(:title, :content, :emergency, :type, :start_date, :end_date,
                                    :user_id, :representative_id, :parent_tag, :child_tag, :complete_function, images: [])
  end

  def correct_article_or_admin
    @article = Article.find(params[:id])
    unless correct_model_or_admin?(@article.user)
      flash[:danger] = 'アクセス権限がありません。'
      redirect_to @article.representative
    end
  end

  def save_incompletes(article)
    Incomplete.transaction do
      @representative.receivers.each do |receiver|
        incomplete = article.incompletes.build(user_id: receiver.id)
        incomplete.save!
      end
    end
  rescue
    flash[:danger] = 'エラーが発生し作業管理チェック機能が追加されませんでした。'
  end
end
