class Admin::DestroyAccountsController < Admin::Base
  skip_before_action :logged_in_user, :sender_only, only: [:edit, :destroy]
  before_action :correct_rep_and_admin, only: [:new, :create]
  before_action :get_user,              only: [:edit, :destroy]
  before_action :valid_user,            only: [:edit, :destroy]
  before_action :check_expiration,      only: [:edit, :destroy]

  def new
  end

  def create
    @user = User.find_by(email: params[:email].downcase)
    if @user
      @user.representative.create_destroy_digest
      @user.representative.send_destroy_account_email(@user)
      flash[:info] = 'アカウント削除用メールを送信しました。'
      redirect_to current_representative
    else
      flash[:danger] = "ユーザーが見つかりません。"
      render 'new'
    end
  end

  def edit
  end

  def destroy
    if params[:password].empty?
      flash.now[:danger] = 'パスワードを入力して下さい。'
      render 'edit'
    elsif @rep && @rep.authenticate(params[:password])
      @rep.destroy
      session.delete(:params_action)
      cookies.delete(:user_id)
      cookies.delete(:remember_token)
      flash[:success] = "代表者アカウント:#{@rep.name} を削除しました。ご利用頂きありがとうございました。"
      redirect_to root_path
    else
      flash[:danger] = 'パスワードが一致しません。'
      render 'edit'
    end
  end

  private

  def correct_rep_and_admin
    rep = Representative.find(params[:representative_id])
    unless current_user.admin?
      flash[:danger] = 'アクセス権限がありません。'
      redirect_to rep
    end
  end

  def get_user
    @rep = User.find_by(email: params[:email]).representative
  end

  # 正しいユーザーか調べる
  def valid_user
    unless @rep && @rep.activated? && @rep.authenticated?(:destroy, params[:id]) # rubocop:disable Airbnb/SimpleUnless
      redirect_to root_path
    end
  end

  def check_expiration
    if @rep.destroy_account_expired?
      flash[:danger] = '退会用ページの有効期限が切れています。'
      redirect_to new_representative_destroy_account_path(@rep)
    end
  end
end
