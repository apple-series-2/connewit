class Admin::UsersController < Admin::Base
  skip_before_action :logged_in_user,   only: [:new, :create]
  skip_before_action :sender_only,      only: [:new, :create, :update]
  before_action :logged_in_correct_rep, only: [:new, :create]
  before_action :correct_user,          only: [:edit, :update, :destroy]
  before_action :correct_user_or_admin, only: [:destroy]
  before_action :current_user_admin,    only: [:add_admin, :delete_admin]

  def index
    current_senders = current_representative.sender_users.page(params[:page])
    current_receivers = current_representative.receivers.page(params[:page])
    keyword = params[:keyword]
    search_sender = params[:sender]
    if keyword.present?
      if search_sender == '情報発信メンバー'
        @users = current_senders.search(keyword).users_order(params[:order])
      else
        @users = current_receivers.search(keyword).users_order(params[:order])
      end
    else
      if search_sender == '情報受信メンバー'
        @users = current_receivers.users_order(params[:order])
      else
        @users = current_senders.users_order(params[:order])
      end
    end
    render layout: 'admin_sidebar'
  end

  def new
    @user = @representative.users.build
    @admin_user = false
  end

  def create
    @user = @representative.users.build(user_params)
    if params[:user][:terms_check] == "0"
      flash.now[:alert] = '利用規約の確認をして下さい。'
      render 'new'
      return
    end
    if request.referer&.include?("/admin_signup")
      @user.admin = true
      @user.sender = true
    end
    if @user.save
      flash[:success] = 'ユーザー登録が完了しました。'
      # representative#showから登録した場合completeビューへ、ホームから登録した場合はnewビューへリダイレクト
      if logged_in?
        redirect_to representative_user_complete_path(@representative, @user)
      else
        log_in(@user)
        redirect_to representative_path(@representative)
      end
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
    render layout: 'admin_sidebar'
  end

  def update
    @user = User.find(params[:id])
    if params[:user][:city_id].present?
      city = City.find_by(weather_id: params[:user][:city_id])
      city ? @user.update(city: city.name, city_id: city.weather_id) : flash[:danger] = '都市の設定に失敗しました。'
      if current_user.sender?
        redirect_to(@representative) && return
      else
        redirect_to(staff_representative_path(@representative)) && return
      end
    else
      return redirect_to staff_representative_path(@representative) unless current_user.sender?
      if @user.update(user_params)
        flash[:success] = 'ユーザー情報を更新しました'
        redirect_to representative_users_path(@representative)
      else
        render 'edit'
      end
    end
  end

  def destroy
    name = @user.name
    @user.destroy
    flash[:warning] = "ユーザー:#{name}を削除しました。"
    redirect_to representative_users_path
  end

  def complete
    @user = User.find(params[:user_id])
  end

  def add_admin
    user = User.find(params[:user_id])
    user.admin = true
    if user.save
      flash[:success] = "ユーザー: #{user.name}に管理者権限を付与しました。"
      redirect_to representative_users_path(@representative)
    else
      flash[:danger] = '管理者権限の付与に失敗しました。'
      redirect_to representative_users_path(@representative)
    end
  end

  def delete_admin
    user = User.find(params[:user_id])
    user.admin = false
    if user.save
      flash[:success] = "ユーザー: #{user.name}に管理者権限を削除しました。"
      redirect_to representative_users_path(@representative)
    else
      flash[:danger] = '管理者権限の削除に失敗しました。'
      redirect_to representative_users_path(@representative)
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :sender, :email, :context, :phone_number, :staff_id,
                                 :password, :password_confirmation, :city, :city_id)
  end

  def correct_user
    @user = User.find(params[:id])
    if @user.sender? && !current_user?(@user)
      flash[:danger] = 'アクセス権限がありません。'
      redirect_to current_representative
    end
  end

  def correct_user_or_admin
    @user = User.find(params[:id])
    if @user.sender? && !current_user?(@user) || !current_user.admin?
      flash[:danger] = 'アクセス権限がありません。'
      redirect_to current_representative
    end
  end
end
