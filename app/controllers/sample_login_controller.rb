class SampleLoginController < ApplicationController
  def sender_login
    user = User.find_by(email: '2-admin@example.com')
    log_in(user)
    redirect_to current_representative
  end

  def receiver_login
    user = User.find_by(staff_id: '2Receiver0')
    log_in(user)
    redirect_to staff_representative_path(current_representative)
  end
end
