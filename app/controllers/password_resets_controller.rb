class PasswordResetsController < ApplicationController
  before_action :get_user, only: [:edit, :update]
  before_action :valid_user, only: [:edit, :update]
  before_action :check_expiration, only: [:edit, :update]

  def new
  end

  def create
    @user = User.find_by(email: params[:email].downcase)
    if @user
      @user.create_reset_digest
      @user.send_password_reset_email
      flash[:info] = 'パスワード再設定用メールを送信しました。'
      redirect_to root_path
    else
      flash.now[:danger] = 'メールアドレスが見つかりません。'
      render 'new'
    end
  end

  def edit
  end

  def update
    if params[:user][:password].empty?
      @user.errors.add(:password, :blank)
      render 'edit'
    elsif @user.update_attributes(user_params)
      log_in @user
      @user.update_attribute(:reset_digest, nil)
      flash[:success] = 'パスワードを再設定しました。'
      redirect_to @user.representative
    else
      render 'edit'
    end
  end

  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  def get_user
    @user = User.find_by(email: params[:email])
  end

  # 正しいユーザーか調べる
  def valid_user
    @rep = @user.representative
    unless @user && @rep.activated? && @user.authenticated?(:reset, params[:id]) # rubocop:disable Airbnb/SimpleUnless
      redirect_to root_path
    end
  end

  def check_expiration
    if @user.password_reset_expired?
      flash[:danger] = 'パスワード再設定の有効期限が切れています。'
      redirect_to new_password_reset_path
    end
  end
end
