class Incomplete < ApplicationRecord
  validates :article_id, presence: true
  validates :user_id, presence: true

  belongs_to :article
  belongs_to :user
  scope :plan_incomplete, -> (article) { find_by('article_id = ?', article.id) }
  scope :completed_list,  -> { where(completed: true) }
  scope :incompleted_list, -> { where(completed: false) }
end
