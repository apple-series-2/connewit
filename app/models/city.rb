class City < ApplicationRecord
  validates :name, presence: true
  validates :weather_id, presence: true
end
