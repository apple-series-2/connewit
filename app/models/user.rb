class User < ApplicationRecord
  include Token
  attr_accessor :remember_token, :reset_token

  has_secure_password
  before_save :downcase_email

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i.freeze
  VALID_ID_REGEX = /\A(?=.*?[a-z])(?=.*?\d)\w+\z/.freeze # 英数字をそれぞれ1文字以上は必ず入れる。大文字を区別する
  VALID_PHONE_REGEX = /\A\d+\z/.freeze

  validates :name,         presence: true,
                           length: { maximum: 50 }
  validates :email,        presence: { if: :sender_user },
                           length: { maximum: 255 },
                           format: { with: VALID_EMAIL_REGEX, allow_blank: true },
                           uniqueness: { case_sensitive: false }
  validates :staff_id,     presence: true,
                           length: { minimum: 6, maximum: 100 },
                           format: { with: VALID_ID_REGEX },
                           uniqueness: true
  validates :password,     presence: true,
                           length: { minimum: 8 },
                           allow_nil: true
  validates :phone_number, length: { minimum: 10, maximum: 11 },
                           format: { with: VALID_PHONE_REGEX },
                           allow_blank: true
  enum sender: { sender: true, receiver: false }

  has_many :articles,    dependent: :destroy
  has_many :members,     dependent: :destroy
  has_many :incompletes, dependent: :destroy
  has_many :plans, -> { plans }, foreign_key: 'user_id',
                                 class_name: 'Article'
  belongs_to :representative

  scope :admin_users,  -> { where(admin: true) }
  scope :sender_users, -> { where(sender: 1) }
  scope :receivers,    -> { where(sender: 0) }
  scope :search,       -> (word) { where('name LIKE ? OR context LIKE ?', "%#{word}%", "%#{word}%") }

  # remember_digestにハッシュ化されたトークンを入れる
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # ユーザーのログイン情報を破棄する
  def forget
    update_attribute(:remember_digest, nil)
  end

  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest, User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  def self.users_order(order)
    order ? order(order) : order('created_at DESC')
  end

  private

  def downcase_email
    self.email = email.downcase if email.present?
  end

  def sender_user
    sender == 'sender'
  end
end
