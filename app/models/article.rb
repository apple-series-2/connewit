class Article < ApplicationRecord
  self.inheritance_column = :_type_disabled
  has_many_attached :images

  validates :title, presence: true, length: { maximum: 50 }
  validates :type, presence: true
  validate :check_plan_days
  enum type: { information: 0, plans: 1 }
  DATE_DEFINE_TODAY = 2
  DEADLINE_DAYS = 2

  belongs_to :user
  belongs_to :representative
  belongs_to :member, optional: true
  has_many   :incompletes, dependent: :destroy
  has_many   :incompleted_list, -> { incompleted_list }, foreign_key: 'article_id',
                                                         class_name: 'Incomplete'
  has_many   :completed_list,   -> { completed_list },   foreign_key: 'article_id',
                                                         class_name: 'Incomplete'
  scope :includes_user_incompletes, -> { includes(:user, :incompletes) }
  scope :plans,             -> { where(type: 1) }
  scope :information,       -> { where(type: 0) }
  scope :emergencies,       -> { where(emergency: true) }
  scope :sender_articles,   -> { joins(:user).merge(User.sender_users) }
  scope :of_senders,        -> { where(users: { sender: 'sender' }) }
  scope :child_tags,        -> (main_tag) { where('parent_tag = ?', main_tag) }
  scope :search_child_tags, -> (keyword) { where('child_tag LIKE ?', "#{keyword}%") }
  scope :search,            -> (word) {
                              where('UPPER(title) LIKE ? OR UPPER(content) LIKE ?',
                                    "%#{word.upcase}%", "%#{word.upcase}%")
                            }
  scope :not_short_plans,   -> { where('plan_days > ?', DATE_DEFINE_TODAY) }
  scope :today_plans,       -> {
                              where('start_date <= ? AND end_date >= ?', Date.today, Date.today).
                                or(where('start_date = ?', Date.today))
                            }
  scope :plans_nearing_deadline, -> { where('end_date <= ?', Date.today.next_day(DEADLINE_DAYS)) }
  scope :plans_nearing_start,    -> {
                                   where('start_date <= ?', Date.today.next_day(DEADLINE_DAYS)).
                                     where('start_date >= ?', Date.today)
                                 }
  scope :incompleted_list,  -> { joins(:incompletes).merge(Incomplete.incompleted_list) }
  scope :child_tags_asc,    -> { order(child_tag: :asc) }

  def self.article_order(order)
    return order("articles.created_at DESC") unless order

    if order == 'start_date ASC'
      where('start_date >= ?', Date.today).order('start_date ASC')
    elsif order == 'end_date ASC'
      where('end_date >= ?', Date.today).order('start_date ASC')
    else
      order(order)
    end
  end

  def validate_tag
    if parent_tag.blank? && child_tag.present?
      false
    else
      true
    end
  end

  def plan_days_value
    (end_date - start_date).to_i
  end

  def check_plan_days
    if start_date && end_date && start_date > end_date
      errors.add(:end_date, "は、開始日以降の日付を設定して下さい。")
    end
  end

  # ユーザーが作業を完了してるか確認
  def completed_as(user)
    incompletes.find_by(user_id: user.id).completed?
  end

  # 自身が予定であることを確認し、それが完了していたらtrueを返す
  def complete_plan?
    return false unless type == 'plans'
    plan_incomp = incompletes.first
    plan_incomp.present? && plan_incomp.completed?
  end
end
