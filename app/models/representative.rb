class Representative < ApplicationRecord
  include Token
  attr_accessor :activation_token, :destroy_token

  has_secure_password
  before_save   :downcase_email
  before_create :create_activation_digest

  validates :name, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i.freeze
  validates :email,    presence: true, length: { maximum: 255 },
                       format: { with: VALID_EMAIL_REGEX },
                       uniqueness: { case_sensitive: false }
  validates :password, presence: true, length: { minimum: 8 }

  has_many :users,    dependent: :destroy
  has_many :articles, dependent: :destroy
  has_many :admin_users,     -> { admin_users }, foreign_key: "representative_id",
                                                 class_name: 'User'
  has_many :sender_users,    -> { sender_users },    foreign_key: 'representative_id',
                                                     class_name: 'User'
  has_many :receivers,       -> { receivers },       foreign_key: 'representative_id',
                                                     class_name: 'User'
  has_many :information,     -> { information },     foreign_key: 'representative_id',
                                                     class_name: 'Article'
  has_many :plans,           -> { plans },           foreign_key: 'representative_id',
                                                     class_name: 'Article'
  has_many :emergency_articles, -> { emergencies },  foreign_key: 'representative_id',
                                                     class_name: 'Article'
  has_many :short_plans,     -> { short_plans },     foreign_key: 'representative_id',
                                                     class_name: 'Article'
  has_many :not_short_plans, -> { not_short_plans }, foreign_key: 'representative_id',
                                                     class_name: 'Article'
  scope :includes_users, -> { includes(:users) }

  def activate
    update_columns(activated: true, activated_at: Time.zone.now)
  end

  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  def create_destroy_digest
    self.destroy_token = Representative.new_token
    update_attribute(:destroy_digest, Representative.digest(destroy_token))
    update_attribute(:destroy_sent_at, Time.zone.now)
  end

  def send_destroy_account_email(user)
    UserMailer.destroy_account(user).deliver_now
  end

  def destroy_account_expired?
    destroy_sent_at < 2.hours.ago
  end

  private

  def downcase_email
    self.email = email.downcase
  end

  def create_activation_digest
    self.activation_token  = User.new_token
    self.activation_digest = User.digest(activation_token)
  end
end
