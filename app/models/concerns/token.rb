module Token
  extend ActiveSupport::Concern

  included do
    class << self
      # ランダムトークンを返す
      def new_token
        SecureRandom.urlsafe_base64
      end

      # 渡された文字列のハッシュ値を返す
      def digest(string)
        cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
        BCrypt::Password.create(string, cost: cost)
      end
    end

    # 渡されたトークンがダイジェストと一致したらtrueを返す
    def authenticated?(attribute, token)
      digest = send("#{attribute}_digest")
      return false if digest.nil?
      BCrypt::Password.new(digest).is_password?(token)
    end
  end
end
