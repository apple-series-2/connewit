module UsersHelper
  # ログインユーザーがadminのみリンク表示
  def button_to_change_admin(user)
    if user.admin?
      link_to '管理者権限を削除', representative_user_delete_admin_path(user.representative, user),
              data: { confirm: "ユーザー: #{user.name}に管理者権限を削除します。\n本当によろしいですか？" },
              class: 'admin-deprivation_authority'
    else
      if user.sender?
        link_to '管理者権限を付与', representative_user_add_admin_path(user.representative, user),
                data: { confirm: "ユーザー: #{user.name}に管理者権限を付与します
                                 (必要なユーザーのみに管理者権限を与えるよう注意して下さい)\n
                                 本当によろしいですか？" },
                class: 'admin-grant_authority'
      end
    end
  end

  # 対象のユーザーがsenderの場合は本人のみ、receiverの場合はログインユーザーがsenderの場合のみリンク表示
  def button_to_delete_user(user)
    if (user.sender? && current_user?(user) || current_user.admin?) || (!user.sender? && current_user.sender?)
      link_to 'ユーザーを削除', representative_user_path(current_representative, user),
              method: :delete,
              data: { confirm: "ユーザー: #{user.name}を削除します。\n本当によろしいですか？" },
              class: 'user-delete_button'
    end
  end
end
