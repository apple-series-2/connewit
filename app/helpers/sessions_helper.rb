module SessionsHelper
  def current_user
    if session[:user_id]
      @current_user ||= User.find_by(id: session[:user_id])
    elsif cookies.signed[:user_id]
      user = User.find_by(id: cookies.signed[:user_id])
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        log_in_representative user.representative
        @current_user = user
      end
    end
  end

  def current_representative
    if session[:representative_id]
      rep_id = session[:representative_id]
      @current_representative ||= Representative.find_by(id: rep_id)
    elsif current_user
      @current_representative ||= current_user.representative
    end
  end

  def current_user?(user)
    user == current_user
  end

  def current_representative?(representative)
    representative == current_representative
  end

  # 正しいユーザー、もしくは管理者であればtrueを返す
  def correct_model_or_admin?(user)
    logged_in? && current_representative?(user.representative) && current_user?(user) || current_user.admin?
  end

  # ユーザーのセッションを永続化する
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

  def log_in(user)
    if user.representative && user.representative.activated?
      session[:user_id] = user.id
      session[:representative_id] = user.representative.id
    end
  end

  def log_in_representative(rep)
    session[:representative_id] = rep.id if rep.activated?
  end

  # 永続化cookieを削除する
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  def log_out
    forget(current_user)
    session.delete(:user_id)
    session.delete(:representative_id)
    session.delete(:params_action)
    @current_user = nil
  end

  def logged_in?
    !current_user.nil? && logged_in_rep?
  end

  def logged_in_rep?
    !current_representative.nil?
  end
end
