module ApplicationHelper
  def full_title(title = '')
    basetitle = 'ConneWat'
    if title.present?
      "#{basetitle} | #{title}"
    else
      basetitle
    end
  end

  def bootstrap_alert(key)
    case key
    when "alert"
      "danger"
    when "notice"
      "info"
    when "error"
      "danger"
    else
      key
    end
  end

  def indicate_keyword(model_name)
    if params[:keyword].present?
      tag.div(class: 'indicate_keyword-container') do
        concat tag.span "検索ワード: #{params[:keyword]}", class: 'indicate_keyword'
        if model_name == 'articles'
          concat link_to('キーワードを削除', representative_articles_path(current_representative, type: params[:type], keyword: nil))
        else
          concat link_to('キーワードを削除', representative_users_path(current_representative, sender: params[:sender], keyword: nil))
        end
      end
    end
  end

  def staff_indicate_keyword(model_name)
    if params[:keyword].present?
      tag.div(class: 'indicate_keyword-container d-flex') do
        concat tag.p "検索ワード: #{params[:keyword]}", class: 'indicate_keyword'
        if model_name == 'articles'
          concat link_to('キーワードを削除',
                         staff_representative_articles_path(current_representative, type: params[:type], keyword: nil))
        else
          concat link_to('キーワードを削除',
                         staff_representative_users_path(current_representative, sender: params[:sender], keyword: nil))
        end
      end
    end
  end

  def sidebar_link_item(name, path, option = nil) # アクティブ担ってるリンクは色を変える
    class_name = 'sidebar_link'
    class_name << 'active' if current_page?(path)

    tag.li(class: class_name) do
      link_to name, path, option
    end
  end

  def contents_link_item(name, path, key) # アクティブ担ってるリンクは色を変える
    class_name = 'contents_link'
    class_name << 'active' if params[:type] == key

    link_to name, path, class: class_name
  end
end
