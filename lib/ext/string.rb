class String
  def abridgement_strings(n)
    bytesize > n ? "#{byteslice(0, n).scrub('')}…" : "#{self}"
  end
end
