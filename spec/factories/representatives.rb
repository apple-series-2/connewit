FactoryBot.define do
  factory :representative do
    sequence(:name) { |n| "ExampleRep#{n}" }
    sequence(:email) { |n| "example#{n}@example.com" }
    password { "password" }
    password_confirmation { "password" }
    activated { true }
    activated_at { "#{Time.zone.now}" }

    trait :invalid do
      name { nil }
    end

    trait :not_activate do
      activated { false }
      activated_at { nil }
    end
  end
end
