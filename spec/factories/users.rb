FactoryBot.define do
  factory :user do
    sequence(:name) { |n| "ExampleMan#{n}" }
    sequence(:email) { |n| "example#{n}@example.com" }
    sequence(:staff_id) { |n| "example#{n}" }
    sender { 'sender' }
    context { 'example部署' } # rubocop:disable all
    phone_number { '08011112222' }
    password { "password" }
    password_confirmation { "password" }
    city { '東京' }
    city_id { 1850147 }

    trait :invalid do
      name { nil }
    end

    trait :admin do
      admin { true }
      sender { 'sender' }
    end
  end
end
