FactoryBot.define do
  require 'date'

  factory :article do
    sequence(:title) { |n| "testTitle#{n}" }
    content { 'testContent' }
    type { 'information' }
    start_date { Date.tomorrow }
    end_date { Date.today.next_day(5) }
    parent_tag { 'test-tag' }
    child_tag { 'test-childtag' }

    after(:build) do |article|
      article.images.attach(io: File.open(Rails.root + 'public/samplejpg1.jpg'), filename: 'samplejpg1.jpg')

      if article.end_date && article.start_date
        article.plan_days = (article.end_date - article.start_date).to_i
        article.save
      end
    end

    trait :invalid do
      title { nil }
    end

    trait :plans do
      type { 'plans' }
      start_date { Date.tomorrow }
      end_date { Date.today.next_day(2) }
      complete_function { true }
      after(:create) do |article|
        create(:incomplete, article: article, user: article.user)
      end
    end

    trait :complete_plans do
      type { 'plans' }
      start_date { Date.tomorrow }
      end_date { Date.today.next_day(2) }
      complete_function { true }
      after(:create) do |article|
        create(:incomplete, article: article, user: article.user, completed: true)
      end
    end

    trait :information_complete do
      complete_function { true }
      after(:create) do |article|
        2.times do |n|
          create(:incomplete, article: article, user: create(:user, name: "receiver_#{n}", sender: 'receiver',
                                                                    representative: article.representative))
        end
      end
    end
  end
end
