FactoryBot.define do
  factory :city do
    name { '東京' }
    weather_id { 1850147 }

    trait :nagoya do
      name { '名古屋' }
      weather_id { 1856057 }
    end
  end
end
