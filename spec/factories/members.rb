FactoryBot.define do
  factory :member do
    sequence(:name) { |n| "testMember#{n}" }
  end
end
