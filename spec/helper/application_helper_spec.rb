require 'rails_helper'

describe ApplicationHelper, type: :helper do
  describe "full_titleメソッドテスト" do
    let(:base_title) { "ConneWat" }

    context 'titleがセットされている場合' do
      it 'base_title | titleでタイトルがセットされる' do
        expect(full_title("テスト")).to eq "#{base_title} | テスト"
      end
    end

    context 'titleがセットされていない場合' do
      it 'base_titleがセットされる' do
        expect(full_title(nil)).to eq "#{base_title}"
      end
    end
  end
end
