require 'rails_helper'

feature 'Users', type: :feature do
  let(:rep) { create(:representative) }
  let(:user) { create(:user, representative: rep) }

  feature 'ユーザー一覧画面' do
    let(:rep2) { create(:representative) }
    let(:user2) { create(:user, representative: rep2) }
    let!(:sender) { create(:user, name: 'fast-senderMan', created_at: Date.yesterday, representative: rep) }
    let!(:receiver) do
      create(:user, name: 'fast-receiverMan',
                    created_at: Date.yesterday,
                    sender: 'receiver',
                    representative: rep)
    end
    let!(:receiver2) { create(:user, name: 'receiverMan', sender: 'receiver', representative: rep) }

    background do
      feature_sign_in(user)
      visit representative_users_path(rep)
    end

    scenario '自分が属するアカウントの情報発信ユーザーが降順ですべて表示される' do
      expect(all('.user-container').last).to have_content 'fast-senderMan'
      expect(all('.user-container').size).to eq 2
      expect(page).not_to have_content user2.name
    end

    scenario '情報受信ユーザーをクリックすると一覧が表示される' do
      click_link '情報受信ユーザー'
      expect(page).to have_content('receiverMan', count: 2)
    end

    scenario 'userパーシャルに正しい情報が記載されている' do
      within("#user-#{User.first.id}") do
        expect(page).to have_content User.first.name
        expect(page).to have_content User.first.sender_i18n
        expect(page).to have_content User.first.context
        expect(page).to have_content User.first.email
        expect(page).to have_content User.first.created_at.strftime('%Y/%m/%d')
      end
    end

    scenario '登録を押すと新規登録画面に移る' do
      click_link 'ユーザー登録を行う'
      expect(current_path).to eq new_representative_user_path(rep)
    end

    scenario '情報発信ユーザー検索が機能する' do
      fill_in 'keyword', with: 'fast-sender'
      click_button '検索'
      expect(page).to have_content 'fast-senderMan'
      expect(expect(all('.user-container').size).to(eq 1))
      expect(page).to have_content "検索ワード: fast-sender"
      expect(page).to have_content "キーワードを削除"
    end

    scenario '情報受信ユーザー検索が機能する' do
      select '情報受信メンバー', from: 'sender'
      fill_in 'keyword', with: 'fast-receiver'
      click_button '検索'
      expect(page).to have_content 'fast-receiverMan'
      expect(expect(all('.user-container').size).to(eq 1))
    end

    scenario '検索がヒットしない場合正しい画面が表示され、キーワードを削除を押すと全件表示に戻る' do
      select '情報受信メンバー', from: 'sender'
      fill_in 'keyword', with: 'test-example-invalid-keyword'
      click_button '検索'
      click_link 'キーワードを削除'
      expect(page).to have_content 'fast-receiverMan'
      expect(expect(all('.user-container').size).to(eq 2))
    end
  end

  feature '新規登録画面' do
    background do
      feature_sign_in(user)
      visit new_representative_user_path(rep)
    end

    context '入力が正しい場合' do
      scenario '登録が完了し完了画面に移る' do
        expect do
          fill_in '氏名', with: 'test'
          fill_in 'メールアドレス', with: 'test@example.com'
          fill_in '部署・所属名', with: '店舗'
          fill_in '電話番号', with: '09011112222'
          fill_in 'ユーザーID', with: 'test123'
          fill_in 'パスワード', with: 'password'
          fill_in 'パスワード(確認)', with: 'password'
          check 'user_terms_check'
          click_button '登録'
        end.to change(User, :count).by(1)

        expect(current_path).to eq representative_user_complete_path(rep, User.last)
        expect(page).to have_content User.last.name
        expect(page).to have_content '情報発信メンバー'
        expect(page).to have_content User.last.email
        expect(page).to have_content User.last.context
        expect(page).to have_content User.last.phone_number
        expect(page).to have_content User.last.staff_id
      end
    end

    scenario '管理者ユーザー登録リンクが機能している' do
      click_link 'こちら'
      expect(current_path).to eq representative_admin_signup_path(rep)
    end

    context '入力が正しくない場合' do
      scenario '登録されない' do
        expect { click_button '登録' }.to not_change(User, :count)
      end
    end

    context '規約に同意していない場合' do
      scenario '登録されない' do
        expect do
          fill_in '氏名', with: 'test'
          fill_in 'メールアドレス', with: 'test@example.com'
          fill_in '部署・所属名', with: '店舗'
          fill_in '電話番号', with: '09011112222'
          fill_in 'ユーザーID', with: 'test123'
          fill_in 'パスワード', with: 'password'
          fill_in 'パスワード(確認)', with: 'password'
          click_button '登録'
        end.to not_change(User, :count)
      end
    end
  end

  feature 'ユーザー編集画面' do
    background do
      feature_sign_in(user)
      visit edit_representative_user_path(rep, user)
    end

    scenario '正しいユーザー編集画面が表示される' do
      expect(page).to have_content user.name
    end

    scenario '正しく入力すると変更が反映されユーザー一覧に遷移する' do
      fill_in '氏名', with: 'change'
      click_button '更新'
      expect(current_path).to eq representative_users_path(rep)
      expect(page).to have_content 'change'
    end

    scenario '誤って入力すると変更が反映されずエラーメッセージが表示される' do
      fill_in '氏名', with: ''
      click_button '更新'
      expect(page).to have_content 'ユーザー編集'
      expect(page).to have_selector '#error_explanation'
      visit representative_users_path(rep)
      expect(page).to have_content user.name
    end
  end
end
