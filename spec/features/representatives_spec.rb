require 'rails_helper'

feature 'Representatives', type: :feature do
  let!(:user) { create(:user, representative: rep) }
  let(:rep) { create(:representative) }
  let(:rep2) { create(:representative) }

  feature 'トップ画面' do
    let!(:today_article) do
      create(:article, title: 'today-a', start_date: Date.today, end_date: Date.tomorrow,
                       user: user, representative: rep)
    end
    let!(:end_article) do
      create(:article, title: 'end-a', start_date: Date.today.prev_day(6), end_date: Date.tomorrow,
                       user: user, representative: rep)
    end
    let!(:start_article) do
      create(:article, title: 'start-a', start_date: Date.tomorrow, end_date: Date.today.next_day(6),
                       user: user, representative: rep)
    end
    let!(:tokyo) { create(:city) }
    let!(:chiba) { create(:city, name: '千葉', weather_id: 2113015) }

    background do
      feature_sign_in(user)
      visit representative_path(rep)
    end

    it 'city名とcity情報を変更するフォームが表示されている' do
      expect(find('.rep-show_container')).to have_content user.city
      expect(find('.rep-show_container .weather-form-input')).to have_selector '#user_city_id'
    end

    it 'cityを選択し変更を押すとuserのcity情報が更新され元のページにリダイレクトする' do
      select '千葉', from: 'user_city_id'
      click_button '変更する'
      visit representative_path(rep)
      expect(user.reload.city_id).to eq 2113015
    end

    context '完了済み、未完了それぞれの予定がある時' do
      let!(:plans) { create(:article, :plans, title: 'plans', user: user, representative: rep) }
      let!(:plans_complete) { create(:article, :complete_plans, title: 'complete_plans', user: user, representative: rep) }

      it 'それぞれの予定partialに正しい情報が表示されている' do
        visit current_path
        expect(find("#start-plan_list #article-#{plans.id}")).to have_content plans.title
        expect(find("#start-plan_list #article-#{plans.id}")).to have_content plans.user.name
        expect(find("#start-plan_list #article-#{plans.id}")).to have_content plans.start_date
        expect(find("#start-plan_list #article-#{plans.id}")).to have_content "未完了"
        expect(find("#start-plan_list #article-#{plans.id}")).to have_button "完了済にする"
        expect(find("#start-plan_list #article-#{plans_complete.id}")).to have_content plans_complete.title
        expect(find("#start-plan_list #article-#{plans_complete.id}")).to have_content "完了済み"
        expect(find("#start-plan_list #article-#{plans_complete.id}")).to have_button "未完了に戻す"
      end
    end

    context '皆の作業を見る機能をつけた予定がある時' do
      let!(:info_complete) do
        create(:article, :information_complete, title: 'information_complete',
                                                user: user,
                                                representative: rep)
      end

      it 'それぞれの予定partialに正しい情報が表示されている' do
        visit current_path
        expect(find("#start-plan_list #article-#{info_complete.id}")).to have_content "皆の作業状態を見る"
      end
    end

    context '今日の予定' do
      let!(:today_article_list) do
        create_list(:article, 4, title: 'today-a',
                                 start_date: Date.today,
                                 end_date: Date.tomorrow,
                                 user: user,
                                 representative: rep)
      end

      it '開始日~締日が今日であれば[今日の予定]に表示される' do
        visit current_path
        expect(Article.today_plans.count).to eq 6
        expect(find('#today-list')).to have_content 'today-a', count: 3
        expect(find('#today-list')).to have_content 'end-a', count: 1
        expect(find('#today-list')).to have_selector 'td', count: 16
      end
    end

    it '期限間近の予定が計1件表示されている' do
      expect(find('#end-plan_list')).to have_content('end-a', count: 1)
      expect(find('#end-plan_list')).to have_selector 'td', count: 4
    end

    it '開始間近の予定が計2件表示されている' do
      expect(find('#today-list')).to have_content('today-a', count: 1)
      expect(find('#start-plan_list')).to have_content('start-a', count: 1)
      expect(find('#start-plan_list')).to have_selector 'td', count: 8
    end

    context '緊急情報がある時' do
      let!(:emergency) { create_list(:article, 5, title: 'emergency_title', emergency: true, user: user, representative: rep) }

      scenario '緊急情報が存在する場合のみ緊急情報一覧に全て表示される' do
        expect(find('#emergency-list')).not_to have_selector '.emergency-plans_container'
        visit current_path
        expect(find('#emergency-list')).to have_content('emergency_title', count: 5)
        expect(find('#emergency-list')).to have_selector 'td', count: 20
      end
    end
  end

  feature '新規登録画面' do
    include ActiveJob::TestHelper
    background { visit new_representative_path }

    context '入力が正しい場合' do
      scenario 'サインアップに成功する' do
        perform_enqueued_jobs do
          expect do
            fill_in 'representative_name', with: 'Example'
            fill_in '代表者メールアドレス', with: 'example@example.com'
            fill_in 'パスワード', with: 'passward'
            fill_in 'パスワード(確認)', with: 'passward'
            check 'representative_terms_check'
            click_button '登録'
          end.to change(Representative, :count).by(1)
          expect(Representative.last.activated?).to eq false
          expect(page).to have_content 'アカウント有効化のメールを送信しました。'
          expect(current_url).to eq root_url
        end
      end
    end

    context '入力が正しくない場合' do
      scenario '正しく入力しないとDBに登録されない' do
        perform_enqueued_jobs do
          expect do
            fill_in 'representative_name', with: 'Exampletest'
            fill_in 'メールアドレス', with: 'example@example.com'
            fill_in 'パスワード', with: 'passward'
            click_button '登録'
          end.to not_change(Representative, :count)
        end
      end

      scenario '規約同意にチェックしないとDBに登録されない' do
        perform_enqueued_jobs do
          expect do
            fill_in 'representative_name', with: 'Example'
            fill_in '代表者メールアドレス', with: 'example@example.com'
            fill_in 'パスワード', with: 'passward'
            fill_in 'パスワード(確認)', with: 'passward'
            click_button '登録'
          end.to not_change(Representative, :count)
        end
      end
    end
  end

  feature '管理者ユーザー登録画面' do
    context '認証済みの場合' do
      background do
        feature_certification_as(rep)
        visit representative_admin_signup_path(rep)
      end

      it '正しく表示される' do
        expect(page).to have_content '管理者ユーザー登録'
        expect(page).to have_content rep.name
      end

      it 'adminユーザーが登録できる' do
        expect do
          fill_in '氏名', with: 'test'
          fill_in 'メールアドレス', with: 'test@example.com'
          fill_in '部署・所属名', with: '店舗'
          fill_in '電話番号', with: '09011112222'
          fill_in 'ユーザーID', with: 'test123'
          fill_in 'パスワード', with: 'password'
          fill_in 'パスワード(確認)', with: 'password'
          check('user_terms_check')
          click_button '登録'
        end.to change(User, :count).by(1)
        expect(User.last.admin?).to eq true
        expect(User.last.sender?).to eq true
      end
    end

    context '認証してない場合' do
      before { visit representative_admin_signup_path(rep) }

      it '認証画面に遷移する' do
        expect(current_path).to eq representative_login_path
      end
    end

    context 'すでに管理者ユーザーが存在する場合' do
      let!(:admin_user) { create(:user, :admin, representative: rep) }

      background do
        feature_certification_as(rep)
        visit representative_admin_signup_path(rep)
      end

      it 'メッセージが表示されユーザーログインパスへ移動する' do
        expect(page).to have_content '既に管理者ユーザーが作成されています。'
        expect(current_path).to eq login_path
      end
    end

    context '正しい代表者でない場合' do
      background do
        feature_certification_as(rep2)
        visit representative_admin_signup_path(rep)
      end

      it 'エラーが表示されroot_pathに遷移する' do
        expect(page).to have_content 'アクセス権限がないサイトへのアクセスです。'
        expect(current_path).to eq root_path
      end
    end
  end

  feature 'Representativeホーム画面' do
    background do
      feature_sign_in(user)
      visit representative_path(rep)
    end

    feature 'header,トップバー' do
      scenario 'headerが表示されている' do
        expect(page).to have_selector '.header'
      end

      scenario 'ユーザー名が表示されている' do
        expect(page).to have_selector '.header', text: user.name
      end
    end

    feature 'サイドバー' do
      scenario 'リンクが正しく機能している' do
        # click_link '連絡情報'
        # expect(current_path).to eq representative_articles_path(rep)
        # click_link '予定一覧'
        # expect(current_path).to eq representative_articles_path(rep)
        # click_link '予定・連絡情報の登録'
        # expect(current_path).to eq new_representative_article_path
        # click_link 'メンバーの登録'
        # expect(current_path).to eq new_representative_user_path
        # click_link 'メンバー一覧'
        # expect(current_path).to eq representative_users_path(rep)
      end
    end
  end
end
