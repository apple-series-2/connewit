require 'rails_helper'

feature 'Sessions', type: :feature do
  subject { page }

  let(:rep) { create(:representative) }
  let!(:user) { create(:user, representative: rep) }
  let!(:receiver) { create(:user, representative: rep, sender: 'receiver') }

  # ユーザーログイン画面
  feature 'ユーザーログイン画面' do
    background { visit login_path }

    # context '正しく入力したとき' do
    #   background do
    #     fill_in 'メールアドレス', with: user.email
    #     fill_in 'パスワード',     with: user.password
    #     click_button 'ログイン'
    #   end

    #   scenario 'ログイン後リダイレクトしメッセージが表示される' do
    #     expect(current_path).to eq representative_path(rep)
    #     is_expected.to have_content user.name
    #     is_expected.to have_content 'ログインしました。'
    #   end
    # end

    context 'ログインユーザーがreceiverの場合' do
      background do
        fill_in 'ID', with: receiver.staff_id
        fill_in 'パスワード', with: receiver.password
        click_button 'ログイン'
      end

      scenario 'staff/representative#showにリダイレクトする' do
        expect(current_path).to eq staff_representative_path(rep)
        is_expected.to have_content receiver.name
        is_expected.to have_content 'ログインしました。'
      end
    end

    context '誤った入力をした場合' do
      background do
        fill_in 'メールアドレス', with: user.email
        click_button 'ログイン'
      end

      scenario 'ページ遷移せずアラートメッセージが表示される' do
        is_expected.to have_selector '.alert-danger', text: 'メールアドレス(ID)もしくはパスワードに誤りがあります。'
        is_expected.to have_current_path login_path
      end

      scenario 'ページ遷移時にflashメッセージが残存しない' do
        click_on '新規会員登録'
        is_expected.not_to have_selector '.alert-danger'
      end
    end

    context 'ユーザーに紐付いている代表者アカウントが有効化されてない場合' do
      let(:rep2) { create(:representative, :not_activate) }
      let!(:user2) { create(:user, representative: rep2) }

      background do
        fill_in 'メールアドレス', with: user2.email
        fill_in 'パスワード', with: user2.password
        click_button 'ログイン'
      end

      scenario 'エラーメッセージが表示されroot_pathへリダイレクトされる' do
        is_expected.to have_content '代表者アカウントが有効化されていません。'
        is_expected.to have_current_path root_path
      end
    end

    context 'before_actionに引っかかりログイン画面に飛ばされた場合' do
      it 'ログイン後に元のリクエストしたパスへ遷移する' do
        visit representative_articles_path(rep)
        expect(current_path).to eq login_path
        fill_in 'メールアドレス', with: user.email
        fill_in 'パスワード', with: user.password
        click_button 'ログイン'
        expect(current_path).to eq representative_articles_path(rep)
      end
    end
  end

  # 代表者認証画面
  feature '代表者認証画面' do
    background { visit representative_login_path }

    context '正しく入力した場合' do
      background do
        fill_in 'メールアドレス', with: rep.email
        fill_in 'パスワード', with: rep.password
        click_button '認証'
      end

      scenario 'ユーザー登録画面にリダイレクトする' do
        expect(current_path).to eq new_representative_user_path(rep)
        is_expected.to have_content '代表者情報の認証に成功しました'
      end
    end

    context '誤って入力した場合' do
      background do
        fill_in 'メールアドレス', with: ''
        fill_in 'パスワード', with: rep.password
        click_button '認証'
      end

      scenario 'リダイレクトせずエラーメッセージが表示される' do
        expect(current_path).to eq representative_login_path
        is_expected.to have_content 'メールアドレスまたはパスワードに誤りがあります。'
      end
    end
  end
end
