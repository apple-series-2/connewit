require 'rails_helper'

feature 'Staff::Members', type: :feature do
  let(:rep) { create(:representative) }
  let!(:receiver) { create(:user, sender: 'receiver', representative: rep) }
  let(:rep2) { create(:representative) }

  background { feature_sign_in(receiver) }

  feature 'メンバー一覧画面' do
    let!(:members) { create_list(:member, 2, user: receiver) }

    background { visit staff_representative_members_path(rep) }

    scenario '登録済みのメンバーが表示されている' do
      expect(page).to have_content("testMember", count: 2)
      expect(all('.member-partial_container').size).to eq 2
    end

    scenario 'partial内に正しい情報が表示されている' do
      within("#member-partial-#{Member.first.id}") do
        expect(page).to have_content "testMember"
        expect(page).to have_selector '.text-primary'
        expect(page).to have_link '削除'
      end
    end

    # java:trueが必要
    # scenario '削除をクリックするとデータが削除される' do
    #   expect{
    #     page.accept_confirm do
    #       click_link '削除'
    #     end
    #     expect(page).to have_content "メンバーを削除しました。"
    #   }.to change(Member, :count).by(-1)
    # end
  end

  feature 'メンバー登録画面' do
    let!(:members) { create_list(:member, 2, user: receiver) }

    background { visit new_staff_representative_member_path(rep) }

    scenario '登録済みのメンバーが表示されている' do
      expect(page).to have_content("testMember", count: 2)
      expect(all('.new_member-list li').size).to eq 2
    end

    scenario 'メンバーの登録が成功する' do
      expect do
        within(all('.form-group').first) do
          fill_in "名前", with: 'test'
        end
        within(all('.form-group')[2]) do
          fill_in "名前", with: 'test2'
        end
        click_button '登録'
      end.to change(Member, :count).by(2)
    end

    scenario '空欄の項目があっても登録が成功する' do
      expect do
        within(all('.form-group').first) do
          fill_in "名前", with: 'test'
        end
        within(all('.form-group')[2]) do
          fill_in "名前", with: ''
        end
        click_button '登録'
      end.to change(Member, :count).by(1)
    end
  end
end
