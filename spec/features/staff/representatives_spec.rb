require 'rails_helper'

feature 'Staff::Representatives', type: :feature do
  let!(:user) { create(:user, representative: rep) }
  let!(:receiver) { create(:user, sender: 'receiver', representative: rep) }
  let(:rep) { create(:representative) }
  let(:rep2) { create(:representative) }

  feature 'トップ画面' do
    let!(:today_article) do
      create(:article, title: 'today-a', start_date: Date.today, end_date: Date.tomorrow,
                       user: user, representative: rep)
    end
    let!(:end_article) do
      create(:article, title: 'end-a', start_date: Date.today.prev_day(6), end_date: Date.tomorrow,
                       user: user, representative: rep)
    end
    let!(:start_article) do
      create(:article, title: 'start-a', start_date: Date.tomorrow, end_date: Date.today.next_day(6),
                       user: user, representative: rep)
    end
    let!(:tokyo) { create(:city) }
    let!(:chiba) { create(:city, name: '千葉', weather_id: 2113015) }

    background do
      feature_sign_in(receiver)
      visit staff_representative_path(rep)
    end

    it '画面上部にcity名とcity情報を変更するフォームが表示されている' do
      expect(find('.weather-wrapper')).to have_content receiver.city
      expect(find('.weather-wrapper .weather-form-input')).to have_selector '#user_city_id'
    end

    it 'cityを選択し変更を押すとuserのcity情報が更新され元のページにリダイレクトする' do
      select '千葉', from: 'user_city_id'
      click_button '変更する'
      visit staff_representative_path(rep)
      expect(receiver.reload.city_id).to eq 2113015
    end

    context '今日の予定' do
      let!(:today_article_list) do
        create_list(:article, 4, title: 'today-a',
                                 start_date: Date.today,
                                 end_date: Date.tomorrow,
                                 user: user,
                                 representative: rep)
      end

      it '開始日~締日が今日であれば[今日の予定]に表示される' do
        visit current_path
        expect(Article.today_plans.count).to eq 6
        expect(find('#today-list')).to have_content 'today-a', count: 3
        expect(find('#today-list')).to have_content 'end-a', count: 1
        expect(find('#today-list')).to have_selector 'a', text: user.name
        expect(find('#today-list')).to have_content Date.tomorrow
        expect(find('#today-list')).to have_selector 'td', count: 16
      end
    end

    it '期限間近の予定が計１件表示されている' do
      expect(find('#end-plan_list')).to have_content('end-a', count: 1)
      expect(find('#end-plan_list')).to have_link 'end-a'
      expect(find('#end-plan_list')).to have_content(Date.tomorrow, count: 1)
      expect(find('#end-plan_list')).to have_selector 'td', count: 4
    end

    it '開始間近の予定が計２件表示されている' do
      expect(find('#start-plan_list')).to have_content('today-a', count: 1)
      expect(find('#start-plan_list')).to have_content('start-a', count: 1)
      expect(find('#start-plan_list')).to have_selector('a', text: user.name, count: 2)
      expect(find('#start-plan_list')).to have_content(Date.tomorrow, count: 1)
      expect(find('#start-plan_list')).to have_selector 'td', count: 8
    end

    context '緊急情報がある時' do
      let!(:emergency) { create(:article, emergency: true, user: user, representative: rep) }

      scenario 'div.emergency-plans_containerに緊急情報のみ全て表示される' do
        visit current_path
        expect(find('#emergency-list')).to have_content(emergency.title, count: 1)
        expect(find('#emergency-list')).to have_selector('a', text: user.name, count: 1)
        expect(find('#emergency-list')).to have_content(emergency.end_date, count: 1)
        expect(find('#emergency-list')).to have_selector 'td', count: 4
      end
    end

    context '完了済み、未完了それぞれの予定がある時' do
      let!(:plans) { create(:article, :plans, title: 'plans', user: receiver, representative: rep) }
      let!(:plans_complete) { create(:article, :complete_plans, title: 'complete_plans', user: receiver, representative: rep) }

      it 'それぞれの予定partialに正しい情報が表示されている' do
        visit current_path
        expect(find("#start-plan_list #article-#{plans.id}")).to have_content plans.title
        expect(find("#start-plan_list #article-#{plans.id}")).to have_content plans.user.name
        expect(find("#start-plan_list #article-#{plans.id}")).to have_content plans.start_date
        expect(find("#start-plan_list #article-#{plans.id}")).to have_content "未完了"
        expect(find("#start-plan_list #article-#{plans.id}")).to have_button "完了済にする"
        expect(find("#start-plan_list #article-#{plans_complete.id}")).to have_content plans_complete.title
        expect(find("#start-plan_list #article-#{plans_complete.id}")).to have_content "完了済み"
        expect(find("#start-plan_list #article-#{plans_complete.id}")).to have_button "未完了に戻す"
      end
    end

    context '皆の作業を見る機能をつけた予定がある時' do
      let(:info_complete) do
        create(:article, title: 'information_complete', user: user,
                         representative: rep, complete_function: true)
      end
      let!(:incomp) { create(:incomplete, user: receiver, article: info_complete) }

      it 'それぞれの予定partialに正しい情報が表示されている' do
        visit current_path
        expect(find("#start-plan_list #article-#{info_complete.id}")).to have_content "未完了"
        expect(find("#start-plan_list #article-#{info_complete.id}")).to have_button "完了済にする"
      end
    end
  end
end
