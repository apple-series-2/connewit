require 'rails_helper'

feature 'Static_pages', type: :feature do
  feature '#home' do
    background do
      visit root_path
    end

    scenario 'headerが正しく表示されている' do
      expect(page).to have_selector '.header .header-logo'
      expect(page).to have_selector '.header', text: '新規会員登録'
      expect(page).to have_selector '.header', text: 'ログイン'
    end

    scenario 'リンクが正しく機能している' do
      click_on '新規会員登録'
      expect(current_path).to eq new_representative_path
    end
  end
end
