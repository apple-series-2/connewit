require 'rails_helper'

feature 'Articles', type: :feature do
  let!(:user) { create(:user, representative: rep) }
  let(:rep) { create(:representative) }

  background { feature_sign_in(user) }

  feature '投稿一覧画面' do
    let(:rep2) { create(:representative) }
    let(:user2) { create(:user, representative: rep2) }
    let!(:info) { create(:article, title: 'info-test-example', created_at: Date.yesterday, user: user, representative: rep) }
    let!(:info_sub) { create(:article, title: 'info-example', user: user, representative: rep) }
    let!(:information) { create_list(:article, 2, user: user, representative: rep) }
    let!(:plan) { create(:article, title: 'plans-test-example', type: 'plans', user: user, representative: rep) }
    let!(:plans) { create(:article, title: 'plans-test', type: 'plans', user: user, representative: rep) }
    let!(:other_information) { create(:article, title: 'invalid-title', user: user2, representative: rep2) }

    background { visit representative_articles_path(rep) }

    scenario '自分が属するアカウントの連絡情報が降順ですべて表示される' do
      expect(all('.article-container').last).to have_content 'info-test-example'
      expect(all('.article-container').size).to eq 4
      expect(page).not_to have_content 'invalid-title'
    end

    scenario 'スタッフの予定一覧をクリックすると予定一覧が表示される' do
      click_link 'スタッフの予定'
      expect(page).to have_content('plans-test', count: 2)
    end

    scenario 'articleパーシャルに正しい情報が記載されている' do
      within("#article-#{Article.first.id}") do
        expect(page).to have_content Article.first.title
        expect(page).to have_content Article.first.start_date
        expect(page).to have_content Article.first.end_date
        expect(page).to have_content Article.first.content
        expect(page).to have_content Article.first.user.name
        expect(page).to have_content Article.first.created_at.strftime('%Y/%m/%d')
      end
    end

    scenario '登録を押すと新規登録画面に移る' do
      click_link '連絡情報・予定の登録を行う'
      expect(current_url).to eq new_representative_article_url(rep)
    end

    scenario '連絡情報検索が機能する' do
      fill_in 'keyword', with: 'example'
      click_button '検索'
      expect(page).to have_content 'info-test-example'
      expect(all('.article-container').size).to eq 2
      expect(page).to have_content "検索ワード: example"
      expect(page).to have_content "キーワードを削除"
    end

    scenario 'スタッフの予定検索が機能する' do
      select 'スタッフの予定', from: 'type'
      fill_in 'keyword', with: 'test-example'
      click_button '検索'
      expect(page).to have_content 'plans-test-example'
      expect(expect(all('.article-container').size).to(eq 1))
    end

    scenario '検索がヒットしない場合正しい画面が表示され、キーワードを削除を押すと全件表示に戻る' do
      select 'スタッフの予定', from: 'type'
      fill_in 'keyword', with: 'test-example-invalid-keyword'
      click_button '検索'
      click_link 'キーワードを削除'
      expect(page).to have_content 'plans-test-example'
      expect(expect(all('.article-container').size).to(eq 2))
    end

    context '連絡情報一覧でcomplete_function機能を利用している記事がある時' do
      before do
        create(:article, :information_complete, user: user, representative: rep)
        visit current_path
      end

      scenario '該当の記事に"皆の作業状態を見る"リンクが追加される' do
        expect(all('.article-container').first).to have_content '皆の作業状態を見る'
        click_link '皆の作業状態を見る'
        expect(current_path).to eq representative_article_incompletes_path(rep, Article.last)
      end
    end

    context '予定一覧でcomplete_function機能を利用している記事がある時' do
      before do
        create(:article, :plans, title: 'incomplete_plan', representative: rep, user: user)
        create(:article, :complete_plans, title: 'complete_plan', representative: rep, user: user)
      end

      scenario '未完了の場合該当の記事に未完了、完了ボタンが表示される' do
        click_link 'スタッフの予定'
        expect(find('#articles-partial')).to have_content '未完了'
        expect(find('#articles-partial')).to have_content '完了済み'
      end
    end

    # ajaxが必要
    # scenario '並び替えが機能する' do
    #   select '投稿日 昇順', from: 'order'
    #   wait_for_ajax
    #   expect(page).to have_select('order', selected: '投稿日 昇順')
    #   expect(all('.article-container').first).to have_content 'info-test-example'
    #   click_link 'スタッフの予定'
    #   expect(all('.article-container').first).to have_content 'plans-test-example'
    # end

    # scenario '検索時も並べ替えが出来る' do
    #   fill_in 'keyword', with: 'test'
    #   click_button '検索'
    #   select '投稿日 昇順', from: 'order'
    #   wait_for_ajax
    #   expect(all('.article-container').first).to have_content 'info-test-example'
    #   expect(expect(all('.article-container').size).to eq 2)
    # end
  end

  feature '投稿詳細画面' do
    let!(:info) { create(:article, title: 'info-test-example', user: user, representative: rep) }

    background do
      feature_sign_in(user)
      visit representative_article_path(rep, info)
    end

    it '正しい情報が表示されている' do
      expect(page).to have_content info.title
      expect(page).to have_content info.content
      expect(page).to have_content info.start_date
      expect(page).to have_content info.end_date
      expect(page).to have_content info.parent_tag
      expect(page).to have_content info.child_tag
    end
  end

  feature '投稿編集画面' do
    let!(:info) { create(:article, title: 'info-test-example', user: user, representative: rep) }

    background do
      feature_sign_in(user)
      visit edit_representative_article_path(rep, info)
    end

    context '正しく入力すると' do
      scenario '変更が反映されメッセージと共に詳細ページにリダイレクトする' do
        fill_in 'タイトル', with: '変更しタイトル'
        click_button '更新'
        expect(current_path).to eq representative_article_path(rep, info)
        expect(page).to have_content 'を更新しました。'
        expect(page).to have_content '変更しタイトル'
      end
    end

    context 'アップロードファイル' do
      scenario '名前がすべて表示される' do
        expect(page).to have_content 'samplejpg1'
        within('.article-form_file') { expect(all('li').size).to eq(1) }
      end

      scenario '新しいファイルを追加でき、削除ボタンで該当のファイルを削除できる' do
        attach_file "article_images", "public/samplejpg1.jpg"
        click_button '更新'
        visit edit_representative_article_path(rep, info)
        within('.article-form_file') { expect(all('li').size).to eq(2) }
        first('.article-form_file > li').click_link '削除'
        visit edit_representative_article_path(rep, info)
      end
    end
  end

  feature '新規登録画面', type: :doing do
    let!(:receiver) { create(:user, sender: 'receiver', representative: rep) }
    background { visit new_representative_article_path(rep) }

    context '正しく入力した場合' do
      scenario 'データが保存される' do
        expect {
          fill_in 'タイトル', with: 'test'
          fill_in '本文', with: 'content'
          check '緊急'
          click_button '登録'
        }.to change(Article, :count).by(1)
      end
    end

    context 'check_completeにチェックした場合' do
      scenario 'データが保存される' do
        expect do
          fill_in 'タイトル', with: 'test'
          fill_in '本文', with: 'content'
          check '作業の完了を確認できるようにする'
          click_button '登録'
        end.to change(Article, :count).by(1).and change(Incomplete, :count).by(1)
        expect(Incomplete.last.article_id).to eq Article.last.id
        expect(Incomplete.last.user_id).to eq receiver.id
      end
    end

    context '入力が誤っている場合' do
      scenario 'データは保存されない' do
        expect do
          fill_in '本文', with: 'content'
          click_button '登録'
        end.to not_change(Article, :count)
      end

      scenario 'エラーメッセージを表示' do
        fill_in '本文', with: 'content'
        click_button '登録'

        expect(page).to have_selector('#error_explanation')
      end

      scenario 'end_dateがstart_dateより早い日付の場合エラーが出る' do
        expect do
          fill_in 'タイトル', with: 'test'
          fill_in '開始日', with: '2020/08/02'
          fill_in '終了(締)日', with: '2020/08/01'
          click_button '登録'
        end.to not_change(Article, :count)

        expect(page).to have_content '開始日以降の日付を設定して下さい。'
      end
    end
  end
end
