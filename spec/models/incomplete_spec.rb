require 'rails_helper'

RSpec.describe Incomplete, type: :model do
  let(:rep) { create(:representative) }
  let(:user) { create(:user, representative: rep) }
  let(:article) { create(:article, user: user, representative: rep) }
  let(:incomplete) { create(:incomplete, user: user, article: article) }

  it 'factoryが有効である' do
    expect(incomplete).to be_valid
  end

  it { should validate_presence_of(:article_id) }
  it { should validate_presence_of(:user_id) }
end
