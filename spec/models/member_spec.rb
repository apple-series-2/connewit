require 'rails_helper'

RSpec.describe Member, type: :model do
  let(:rep) { create(:representative) }
  let(:user) { create(:user, representative: rep) }
  let(:member) { create(:member, user: user) }

  it 'ファクトリーが有効である' do
    expect(member).to be_valid
  end

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:user_id) }
end
