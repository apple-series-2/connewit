require 'rails_helper'

RSpec.describe City, type: :model do
  let(:city) { create(:city) }

  it '有効なfactoryである' do
    expect(city).to be_valid
  end

  describe 'バリデーション' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:weather_id) }
  end
end
