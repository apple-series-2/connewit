require 'rails_helper'

RSpec.describe Article, type: :model do
  let(:rep) { create(:representative) }
  let(:user) { create(:user, representative: rep) }
  let(:article) { create(:article, user: user, representative: rep) }

  it 'factoryが有効である' do
    expect(article).to be_valid
  end

  describe 'バリデーション' do
    it { should validate_presence_of(:title) }
    it { should validate_length_of(:title).is_at_most(50) } # .with_message('タイトルは50文字以内で入力してください')
    it { should validate_presence_of(:type) }
    it { should allow_values('information', 'plans').for(:type) }

    it 'typeカラムにenum指定の文字列以外を投げるとArgumentErrorが発生する' do
      expect { build(:article, type: 'test') }.to raise_error ArgumentError
    end

    it 'end_dateにstart_dateより前の日付を設定した場合エラーが発生する' do
      article.end_date = Date.yesterday
      expect(article).not_to be_valid
    end

    it 'end_date、start_dateもしくはその両方が空欄でもエラーにならない' do
      article.end_date = nil
      expect(article).to be_valid
      article.start_date = nil
      expect(article).to be_valid
      article.end_date = Date.today.next_day(5)
      expect(article).to be_valid
    end
  end

  describe 'リレーション' do
    it { should belong_to(:user) }
    it { should belong_to(:representative) }
  end

  describe 'マクロ' do
    it { should define_enum_for(:type).with_values(information: 0, plans: 1) }
  end
end
