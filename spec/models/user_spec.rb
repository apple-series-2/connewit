require 'rails_helper'

RSpec.describe User, type: :model do
  let!(:user) { create(:user, email: 'sample@example.com', representative: rep) }
  let(:rep) { create(:representative) }

  it 'factoryが有効なアカウントである' do
    expect(user).to be_valid
  end

  describe 'バリデーション' do
    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_most(50) }
    it { should validate_length_of(:email).is_at_most(255) }
    it { should validate_presence_of(:staff_id) }
    it { should validate_length_of(:staff_id).is_at_least(6) }
    it { should validate_length_of(:staff_id).is_at_most(100) }
    it { should validate_uniqueness_of(:staff_id) }
    it { should validate_presence_of(:password) }
    it { should validate_length_of(:password).is_at_least(8) }
    it { should validate_length_of(:phone_number).is_at_least(10) }
    it { should validate_length_of(:phone_number).is_at_most(11) }

    it '無効なemailアドレスは受け付けない' do
      invalid_addresses = %w(
        user@example,com user_at_foo.org user.name@example.
        foo@bar_baz.com foo@bar+baz.com
      )
      invalid_addresses.each do |invalid_address|
        user.email = invalid_address
        expect(user).not_to be_valid
      end
    end

    it '登録済みのemailは受け付けない' do
      rep_copy = build(:user, email: 'sample@example.com', representative: rep)
      rep_copy.valid?
      expect(rep_copy.errors[:email]).to include 'はすでに存在します'
    end

    it 'phone_numberがnilもしくは空欄の場合バリデーションをスキップする' do
      user.phone_number = ""
      expect(user).to be_valid
      user.phone_number = nil
      expect(user).to be_valid
    end

    context 'ユーザーがsenderの時' do
      it 'emailが入力必須になる' do
        user.email = nil
        expect(user).not_to be_valid
      end
    end

    context 'ユーザーがreceiverの時' do
      let!(:receiver) { create(:user, sender: 'receiver', representative: rep) }

      it 'email未入力を許可する' do
        receiver.email = nil
        expect(receiver).to be_valid
      end

      it 'email入力時のみformatバリデーションが走る' do
        receiver.email = 'fefe@do,failed'
        expect(receiver).not_to be_valid
      end
    end

    it '無効なIDは受け付けない' do
      invalid_ids = %w(21212112 fafafafae fea12)
      invalid_ids.each do |invalid_id|
        user.staff_id = invalid_id
        expect(user).not_to be_valid
      end
    end

    it '無効な電話番号は受け付けない' do
      invalid_numbers = %w(212121123f 3123-123123 23312_2312)
      invalid_numbers.each do |invalid_number|
        user.phone_number = invalid_number
        expect(user).not_to be_valid
      end
    end
  end

  describe 'リレーション' do
    it { should belong_to(:representative) }
    it { should have_many(:articles) }
  end

  # ダイジェストが存在しない場合のauthenticated?のテスト
  describe "authenticated?メソッドテスト" do
    it "remember_digestがnilならfalseを返す" do
      expect(user.authenticated?(:remember, '')).to eq false
    end
  end
end
