require 'rails_helper'

RSpec.describe Representative, type: :model do
  let(:rep) { create(:representative) }

  it 'factoryが有効なアカウントである' do
    expect(rep).to be_valid
  end

  describe 'バリデーション' do
    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_most(50) }
    it { should validate_presence_of(:email) }
    it { should validate_length_of(:email).is_at_most(255) }
    it { should validate_presence_of(:password) }
    it { should validate_length_of(:password).is_at_least(8) }

    it '無効なemailアドレスは受け付けない' do
      invalid_addresses = %w(
        user@example,com user_at_foo.org user.name@example.
        foo@bar_baz.com foo@bar+baz.com
      )
      invalid_addresses.each do |invalid_address|
        rep.email = invalid_address
        expect(rep).not_to be_valid
      end
    end

    it '登録済みのemailは受け付けない' do
      FactoryBot.create(:representative, email: 'sample@example.com')
      rep_copy = FactoryBot.build(:representative, email: 'sample@example.com')
      rep_copy.valid?
      expect(rep_copy.errors[:email]).to include 'はすでに存在します'
    end
  end

  describe 'リレーション' do
    it { should have_many(:articles) }
    it { should have_many(:users) }
  end
end
