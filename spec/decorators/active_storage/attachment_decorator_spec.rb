require 'rails_helper'

RSpec.describe ActiveStorage::AttachmentDecorator do
  describe 'image_destroy_path' do
    include Rails.application.routes.url_helpers
    let(:user) { create(:user, representative: rep) }
    let(:rep) { create(:representative) }
    let!(:info) { create(:article, user: user, representative: rep) }
    let(:info_decorate) { info.images[0].decorate }
    let(:image_id) { info_decorate.id }

    it 'パスが生成される' do
      expect(info_decorate.image_destroy_path(info)).to eq representative_article_image_path(rep, info, image_id)
    end
  end
end
