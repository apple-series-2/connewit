require 'rails_helper'

RSpec.describe "Incompletes", type: :request do
  let(:rep) { create(:representative) }
  let(:user) { create(:user, representative: rep) }
  let(:article) { create(:article, complete_function: true, user: user, representative: rep) }
  let(:receiver) { create(:user, sender: 'receiver', representative: rep) }
  let(:receiver_2) { create(:user, sender: 'receiver', representative: rep) }
  let!(:complete) { create(:incomplete, user: receiver, article: article, completed: true) }
  let!(:incomplete) { create(:incomplete, user: receiver_2, article: article, completed: false) }

  before { sign_in_as(user) }

  describe 'GET /index' do
    before do
      get representative_article_incompletes_path(rep, article)
    end

    it 'リクエストが成功する' do
      expect(response).to have_http_status(:success)
    end

    context '完了済みのリサーバー' do
      let(:html) { Nokogiri::HTML(response.body) }
      let(:completes_user_doc) { html.css("#completes-user-#{receiver.id}") }

      it '正しく表示されている' do
        expect(completes_user_doc.to_s).to include receiver.name
        expect(completes_user_doc.to_s).to include '完了済'
      end
    end

    context '未完了のリサーバー' do
      let(:html) { Nokogiri::HTML(response.body) }
      let(:completes_user_doc) { html.css("#completes-user-#{receiver_2.id}") }

      it '正しく表示されている' do
        expect(completes_user_doc.to_s).to include receiver_2.name
        expect(completes_user_doc.to_s).to include '未完了'
      end
    end
  end

  describe 'GET /pacth' do
    before do
      patch representative_article_incomplete_path(rep, article, incomplete)
    end

    it 'リクエストに成功する' do
      expect(response.status).to eq 302
    end

    it 'completedがtrueになる' do
      expect(incomplete.reload.completed).to eq true
    end
  end

  describe 'GET /delete' do
    before do
      delete representative_article_incomplete_path(rep, article, complete)
    end

    it 'リクエストに成功する' do
      expect(response.status).to eq 302
    end

    it 'completedがfalseになる' do
      expect(complete.reload.completed).to eq true
    end
  end

  describe 'GET /all_completes_delete' do
    before do
      delete representative_article_all_incompletes_path(rep, article)
    end

    it 'リクエストに成功する' do
      expect(response.status).to eq 302
    end

    it 'articleに属するincompletesが全て削除される' do
      expect(article.incompletes.reload.size).to eq 0
    end
  end
end
