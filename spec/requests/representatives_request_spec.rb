require 'rails_helper'

RSpec.describe "representatives", type: :request do
  shared_examples_for 'リクエストが成功する(200)' do
    it { expect(response.status).to eq(200) }
  end

  shared_examples_for 'リクエストが成功する(302)' do
    it { expect(response.status).to eq(302) }
  end

  let!(:user) { create(:user, representative: rep) }
  let(:rep) { create(:representative) }
  let!(:user2) { create(:user, representative: rep2) }
  let(:rep2) { create(:representative) }

  # GET /show
  describe 'GET /show' do
    context 'リクエストが正常な場合' do
      before do
        sign_in_as(user)
        get representative_path(rep)
      end

      it_behaves_like 'リクエストが成功する(200)'

      it 'ユーザー名が表示されている' do
        expect(response.body).to include user.name
      end
    end

    context 'セッションが正しくない場合' do
      it '別画面へリダイレクトされる' do
        # ログインしてなかった時
        get representative_path(rep)
        expect(response).to redirect_to login_path(path: representative_path(rep))
        # 関係ないアカウントへアクセスした時
        sign_in_as(user)
        expect(session[:representative_id]).to eq user.representative.id
        get representative_path(rep2)
        expect(response).to redirect_to root_path
        # repのみ認証しユーザーログインはまだの時
        delete logout_path
        certification_as(rep)
        get representative_path(rep)
        expect(response).to redirect_to login_path(path: representative_path(rep))
      end
    end

    context 'ajax' do
      let!(:today_article) do
        create(:article, title: 'today-a', start_date: Date.today,
                         end_date: Date.tomorrow, user: user, representative: rep)
      end
      let!(:end_article) do
        create(:article, title: 'end-a', start_date: Date.today.prev_day(6),
                         end_date: Date.tomorrow, user: user, representative: rep)
      end
      let!(:start_article) do
        create(:article, title: 'start-a', start_date: Date.tomorrow,
                         end_date: Date.today.next_day(6), user: user, representative: rep)
      end

      before do
        sign_in_as(user)
        get representative_path(rep)
      end

      context 'params type: todayの場合' do
        before { get representative_path(rep), xhr: true, params: { type: 'today' } }

        it_behaves_like 'リクエストが成功する(200)'

        it '正しいレスポンスが帰ってくる' do
          expect(response.body).to include today_article.title, user.name
        end
      end

      context 'params type: end_planの場合' do
        before { get representative_path(rep), params: { type: 'end_plan' }, xhr: true }

        it_behaves_like 'リクエストが成功する(200)'

        it '正しいレスポンスが帰ってくる' do
          expect(response.body).to include end_article.title, user.name
        end
      end

      context 'params type: start_planの場合' do
        before { get representative_path(rep), xhr: true, params: { type: 'start_plan' } }

        it_behaves_like 'リクエストが成功する(200)'

        it '正しいレスポンスが帰ってくる' do
          expect(response.body).to include start_article.title, user.name
        end
      end
    end
  end

  # GET /new
  describe 'GET /new' do
    before { get new_representative_path }

    it_behaves_like 'リクエストが成功する(200)'
  end

  # POST
  describe 'POST' do
    context 'パラメーターが正常な場合' do
      before { post representatives_url, params: { representative: attributes_for(:representative) } }

      it_behaves_like 'リクエストが成功する(302)'

      it 'リダイレクトする' do
        expect(response).to redirect_to root_path
      end
    end

    context 'パラメーターが不正の場合' do
      before { post representatives_url, params: { representative: attributes_for(:representative, :invalid) } }

      it_behaves_like 'リクエストが成功する(200)'

      it 'エラーが表示されページが遷移しない' do
        expect(response.body).to include 'エラーが発生しました。'
      end
    end
  end

  # GET /new_admin_user
  describe 'GET /new_admin_user' do
    before { get representative_admin_signup_path(rep) }

    it_behaves_like 'リクエストが成功する(302)'

    context '正しい代表者で認証してない場合' do
      it '別ページへリダイレクトされる' do
        expect(response).to redirect_to representative_login_path
        certification_as(rep2)
        get representative_admin_signup_path(rep)
        expect(response).to redirect_to root_path
      end
    end

    context 'アカウントが有効化されてない場合' do
      let(:not_act_rep) { create(:representative, :not_activate) }

      it 'root_pathへリダイレクトされる' do
        certification_as(not_act_rep)
        expect(response).to redirect_to root_path
      end
    end

    context 'activation_tokenが不正の場合' do
      it '認証を通過しない' do
        get edit_account_activation_path('invalid_token', email: rep.email)
        expect(session[:representative_id]).to be_falsey
      end
    end

    context '有効化済みの場合' do
      let!(:admin_user) { create(:user, :admin, representative: rep2) }

      it '正しく管理者ユーザー登録画面に遷移する' do
        certification_as(rep)
        expect(response.status).to eq(302)
        expect(response).to redirect_to representative_admin_signup_path(rep)
      end

      it 'すでに管理者ユーザーが存在するとログインページにリダイレクトされる' do
        certification_as(rep2)
        get representative_admin_signup_path(rep2)
        expect(response).to redirect_to login_path
      end
    end
  end
end
