require 'rails_helper'

RSpec.describe "DestroyAccounts", type: :request do
  let(:rep) { create(:representative) }
  let(:user) { create(:user, :admin, representative: rep) }

  describe "GET /new" do
    let(:rep2) { create(:representative) }
    let(:other_user) { create(:user, :admin, representative: rep2) }
    let(:normal_user) { create(:user, representative: rep) }

    it "管理者がログインしている場合アクセスに成功" do
      sign_in_as(user)
      get new_representative_destroy_account_path(rep)
      expect(response).to have_http_status(:success)
      expect(response.body).to include 'サービスの退会メールの送信'
    end

    it 'ログインしていない場合リクエストパスを持ってログイン画面にリダイレクト' do
      get new_representative_destroy_account_path(rep)
      expect(response).to have_http_status(302)
      expect(response).to redirect_to login_path(path: new_representative_destroy_account_path(rep))
    end

    it '正しい代表者アカウントを持ち、なおかつ管理者のユーザーでなければリダイレクト' do
      sign_in_as(other_user)
      get new_representative_destroy_account_path(rep)
      expect(response).to have_http_status(302)
      expect(response).to redirect_to root_path

      sign_in_as(normal_user)
      get new_representative_destroy_account_path(rep)
      expect(response).to have_http_status(302)
      expect(response).to redirect_to representative_path(rep)
    end
  end

  describe "POST" do
    before { sign_in_as(user) }

    context 'パラメータが正しい場合' do
      it "成功する" do
        post representative_destroy_accounts_path(rep, email: user.email)
        expect(response).to have_http_status(302)
        expect(response).to redirect_to rep
      end
    end

    context 'メールアドレスが存在しない場合' do
      it "エラーが出て失敗する" do
        post representative_destroy_accounts_path(rep, email: "")
        expect(response).to have_http_status(:success)
        expect(response.body).to include 'ユーザーが見つかりません。'
      end
    end
  end

  describe "GET /edit" do
    before { rep.create_destroy_digest }

    context 'トークンが正しければ' do
      it 'アクセスが成功する' do
        get edit_representative_destroy_account_path(rep, rep.destroy_token), params: { email: user.email }
        expect(response).to have_http_status(:success)
        expect(response.body).to include 'サービスの退会（最終確認）'
      end
    end

    context 'トークンが正しくなければ' do
      let(:rep2) { create(:representative) }
      before { rep2.create_destroy_digest }

      it 'アクセスが失敗する' do
        get edit_representative_destroy_account_path(rep, rep2.destroy_token), params: { email: user.email }
        expect(response).to have_http_status(302)
        expect(response).to redirect_to root_path
      end
    end

    context '有効期限が切れていると' do
      before do
        rep.destroy_sent_at = 3.hours.ago
        rep.save
      end

      it 'アクセスが失敗する' do
        get edit_representative_destroy_account_path(rep, rep.destroy_token), params: { email: user.email }
        expect(response).to have_http_status(302)
        expect(response).to redirect_to new_representative_destroy_account_path(rep)
      end
    end
  end

  describe "DELETE /destroy" do
    before { rep.create_destroy_digest }

    context 'パスワードが正しければ' do
      it '削除が成功する' do
        delete representative_destroy_account_path(rep, rep.destroy_token, email: user.email, password: user.password)
        expect(response).to have_http_status(302)
        expect(response).to redirect_to root_path
        expect(Representative.all.count).to eq 0
      end
    end

    context 'パスワードが正しくなければ' do
      it '削除が失敗する' do
        delete representative_destroy_account_path(rep, 'faildSmfRgLsmmj5yNEESw', email: user.email, password: user.password)
        expect(response).to have_http_status(302)
        expect(response).to redirect_to root_path
        expect(Representative.all.count).to eq 1
      end
    end
  end
end
