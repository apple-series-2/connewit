require 'rails_helper'

RSpec.describe "Password resets", type: :request do
  let(:user) { create(:user, representative: rep) }
  let(:rep) { create(:representative) }

  before do
    user.create_reset_digest
    get edit_password_reset_path(user.reset_token), params: { email: user.email }
  end

  it 'reset_tokenとreset_digestが設定される' do
    expect(user.reset_token.present?).to eq true
    expect(user.reset_digest.present?).to eq true
  end

  describe 'get password_resets#edit' do
    it 'パスワード再設定画面が表示される' do
      expect(response.status).to eq(200)
      expect(response.body).to include 'パスワード再設定'
    end
  end

  describe 'patch password_resets#update' do
    before do
      patch password_reset_path { params[:id] },
            params: { email: user.email, user: { password: 'password1', password_confirmation: 'password1' } }
    end

    it 'reset_digestが削除される' do
      expect(User.last.email).to eq user.email
      expect(User.last.reset_digest).to eq nil
    end

    it 'パスワードが変更されている' do
      expect(User.last.email).to eq user.email
      expect(User.last.password_digest).not_to eq user.password_digest
    end

    it 'representative#showにリダイレクトされる' do
      expect(response).to redirect_to rep
    end
  end
end
