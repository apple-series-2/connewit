require 'rails_helper'

RSpec.describe "Articles", type: :request do
  let!(:user) { create(:user, email: 'example@example.com', representative: rep) }
  let(:rep) { create(:representative, email: 'example@example.com') }

  shared_examples_for 'リクエストが成功する(200)' do
    it { expect(response.status).to eq(200) }
  end

  shared_examples_for 'リクエストが成功する(302)' do
    it { expect(response.status).to eq(302) }
  end

  describe 'GET /new' do
    context 'ログイン済みの場合' do
      before do
        sign_in_as(user)
        get new_representative_article_path(rep)
      end

      it_behaves_like 'リクエストが成功する(200)'
    end

    context '未ログインの場合' do
      before { get new_representative_article_path(rep) }

      it_behaves_like 'リクエストが成功する(302)'

      it 'リダイレクトする' do
        expect(response).to redirect_to login_path(path: new_representative_article_path(rep))
      end
    end
  end

  describe 'POST' do
    before { sign_in_as(user) }

    context 'リクエストが正しい場合' do
      before do
        post representative_articles_path(rep),
             params: { article: attributes_for(:article, user_id: user.id, representative_id: rep.id) }
      end

      it_behaves_like 'リクエストが成功する(302)'

      it '登録される' do
        expect(Article.all.size).to eq 1
      end

      it 'plan_daysが設定されている' do
        expect(Article.last.plan_days.present?).to eq true
      end

      context 'complete_functionがtrueの場合' do
        let!(:receiver) { create(:user, sender: 'receiver', representative: rep) }

        before do
          post representative_articles_path(rep),
               params: { article: attributes_for(:article, user_id: user.id, representative_id: rep.id, complete_function: "1") }
        end

        it_behaves_like 'リクエストが成功する(302)'

        it 'incompleteも登録される' do
          expect(Article.all.size).to eq 2
          expect(Incomplete.last.article_id).to eq Article.last.id
          expect(Incomplete.last.user_id).to eq receiver.id
        end
      end
    end

    context 'リクエストが正しくない場合' do
      let(:invalid_art) { create(:article, :invalid, user: user) }
      before { post representative_articles_path(rep), params: { article: attributes_for(:article, :invalid, user_id: user.id) } }

      it_behaves_like 'リクエストが成功する(200)'

      it 'child_tagが入力されparent_tagがnilの場合失敗する' do
        post representative_articles_path(rep), params: { article: attributes_for(:article, parent_tag: nil, user_id: user.id) }
        expect(response.status).to eq(200)
      end
    end
  end

  describe 'GET /index' do
    require 'nokogiri'

    let(:rep2)  { create(:representative) }
    let(:user2) { create(:user, representative: rep2) }
    let!(:info) { create(:article, title: 'info-test-example', created_at: Date.yesterday, user: user, representative: rep) }
    let!(:information) { create_list(:article, 2, user: user, representative: rep) }
    let!(:plan) do
      create(:article, title: 'plans-test-example',
                       created_at: Date.yesterday,
                       type: 'plans',
                       user: user,
                       representative: rep)
    end
    let!(:plans) { create_list(:article, 1, type: 'plans', user: user, representative: rep) }
    let(:html)   { Nokogiri::HTML(response.body) }
    let(:article_container_doc) { html.css('.article-container') }

    before do
      sign_in_as(user)
      get representative_articles_path(rep)
    end

    it_behaves_like 'リクエストが成功する(200)'

    it '正しいページが表示されている' do
      expect(response.body).to include 'info-test-example'
      expect(article_container_doc.size).to eq 3
    end

    it 'type:plansを渡すと予定一覧が表示される' do
      get representative_articles_path(rep), params: { type: 'スタッフの予定' }
      expect(response.body).to include 'plans-test-example'
      expect(article_container_doc.size).to eq 2
    end

    it 'keywordを渡すとtitleとcontent内で前後一致した情報が表示される' do
      get representative_articles_path(rep), params: { keyword: 'fo-test', type: '連絡情報' }
      expect(response.body).to include 'info-test-example'
      expect(article_container_doc.size).to eq 1
      get representative_articles_path(rep), params: { keyword: 'ns-test', type: 'スタッフの予定' }
      expect(response.body).to include 'plans-test-example'
      expect(article_container_doc.size).to eq 1
    end

    it 'orderを渡すと並び替えされる' do
      get representative_articles_path(rep), params: { order: 'articles.created_at ASC' }
      expect(article_container_doc.first.to_s).to include 'info-test-example'
    end

    it 'typeとorderを渡すと渡されたtypeのarticleで並び替えされる' do
      get representative_articles_path(rep), params: { type: 'スタッフの予定', order: 'articles.created_at ASC' }
      expect(article_container_doc.first.to_s).to include 'plans-test-example'
    end
  end

  describe 'GET /show' do
    let!(:info) { create(:article, user: user, representative: rep) }

    before do
      sign_in_as(user)
      get representative_article_path(rep, info)
    end

    it_behaves_like 'リクエストが成功する(200)'

    it '正しい記事情報が渡されている' do
      expect(response.body).to include info.title
    end
  end

  describe 'GET /edit' do
    let!(:info) { create(:article, user: user, representative: rep) }

    before do
      sign_in_as(user)
      get edit_representative_article_path(rep, info)
    end

    it_behaves_like 'リクエストが成功する(200)'

    it 'articleの情報を持っている' do
      expect(response.body).to include info.title
    end
  end

  describe 'PACHE' do
    let!(:info) { create(:article, user: user, representative: rep) }

    before { sign_in_as(user) }

    context 'パラメータが正しい場合' do
      before { patch representative_article_path(rep, info), params: { article: { title: 'タイトル変更' } } }

      it 'リクエストが成功し変更が反映される' do
        expect(response.status).to eq(302)
        get representative_article_path(rep, info)
        expect(response.body).to include 'タイトル変更'
      end

      it 'リダイレクトする' do
        expect(response).to redirect_to representative_article_path(rep, info)
      end
    end

    context 'パラメータが正しくない場合' do
      before { patch representative_article_path(rep, info), params: { article: { title: '' } } }

      it 'リクエストが成功するが変更は反映されない' do
        expect(response.status).to eq(200)
        get representative_article_path(rep, info)
        expect(response.body).to include 'testTitle'
      end
    end
  end
end
