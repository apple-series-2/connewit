require 'rails_helper'

RSpec.describe "Users", type: :request do
  let(:rep) { create(:representative) }
  let(:user) { create(:user, representative: rep) }

  shared_examples_for 'リクエストが成功する(200)' do
    it { expect(response.status).to eq(200) }
  end
  shared_examples_for 'リクエストが成功する(302)' do
    it { expect(response.status).to eq(302) }
  end

  before { certification_as(rep) }

  describe 'GET /index' do
    require 'nokogiri'

    let(:rep2) { create(:representative) }
    let(:user2) { create(:user, representative: rep2) }
    let!(:sender) { create(:user, name: 'fast-senderMan', created_at: Date.yesterday, representative: rep) }
    let!(:receiver) do
      create(:user, name: 'fast-receiverMan', created_at: Date.yesterday,
                    sender: 'receiver', representative: rep)
    end
    let!(:receiver2) { create(:user, sender: 'receiver', representative: rep) }
    let(:html) { Nokogiri::HTML(response.body) }
    let(:user_container_doc) { html.css('.user-container') }

    before do
      sign_in_as(user)
      get representative_users_path(rep)
    end

    it_behaves_like 'リクエストが成功する(200)'

    it 'その代表者に紐付いたユーザーの情報発信者のみを、登録日時降順で表示されている' do
      expect(user_container_doc.last.to_s).to include 'fast-senderMan'
      expect(response.body).not_to include user2.name
      expect(user_container_doc.size).to eq 2
    end

    it 'sender:receiverを渡すと情報受信者一覧が表示される' do
      get representative_users_path(rep), params: { sender: '情報受信メンバー' }
      expect(response.body).to include 'fast-receiverMan'
      expect(user_container_doc.size).to eq 2
    end

    it 'keywordを渡すとtitleとcontext内で前後一致した情報が表示される' do
      get representative_users_path(rep), params: { keyword: 'rMa', sender: '情報発信メンバー' }
      expect(response.body).to include 'fast-senderMan'
      expect(user_container_doc.size).to eq 1
      get representative_users_path(rep), params: { keyword: 'rMa', sender: '情報受信メンバー' }
      expect(response.body).to include 'fast-receiverMan'
      expect(user_container_doc.size).to eq 1
    end

    it 'orderを渡すと並び替えされる' do
      get representative_users_path(rep), params: { order: 'created_at ASC' }
      expect(user_container_doc.first.to_s).to include 'fast-senderMan'
    end

    it 'senderとorderを渡すと渡されたsenderのuserで並び替えされる' do
      get representative_users_path(rep), params: { sender: '情報受信メンバー', order: 'created_at ASC' }
      expect(user_container_doc.first.to_s).to include 'fast-receiverMan'
    end
  end

  describe 'GET/new' do
    before { get new_representative_user_path(rep) }

    it_behaves_like 'リクエストが成功する(200)'

    it 'ログインしてない場合認証画面へ移る' do
      sign_in_as(user)
      delete logout_path
      get new_representative_user_path(rep)
      expect(response).to redirect_to representative_login_path
    end

    it 'admin:trueを渡すと管理者登録画面に変化する' do
      get representative_admin_signup_path(rep)
      expect(response.body).to include '管理者'
    end
  end

  # create
  describe 'POST' do
    context 'ユーザーログインせず正しく登録した場合' do
      before { post representative_users_path(rep), params: { user: attributes_for(:user) } }

      it_behaves_like 'リクエストが成功する(302)'

      it 'rep#showにリダイレクトする' do
        expect(response).to redirect_to rep
      end
    end

    context 'ユーザーログイン済みで登録した場合' do
      let(:user2) { create(:user, representative: rep) }

      before do
        sign_in_as(user2)
        post representative_users_path(rep), params: { user: attributes_for(:user) }
      end

      it_behaves_like 'リクエストが成功する(302)'

      it '登録後ユーザーの情報を持ってcompleteビューにリダイレクトする' do
        expect(response).to redirect_to representative_user_complete_path(rep, User.last)
        expect(User.last.admin).to eq false
      end
    end

    context 'パラメーターが不正な場合' do
      before do
        post representative_users_path(rep), params: { user: attributes_for(:user, :invalid) }
      end

      it_behaves_like 'リクエストが成功する(200)'
    end
  end

  # GET /edit
  describe 'GET /edit' do
    let(:other_rep) { create(:representative) }
    let(:other_user) { create(:user, representative: other_rep) }

    context 'ログインしてない場合' do
      it 'ログイン画面にリダイレクトする' do
        get edit_representative_user_path(rep, user)
        expect(response).to redirect_to login_path(path: edit_representative_user_path(rep, user))
      end
    end

    context 'ログインしている代表者アカウントと違う代表者のユーザー編集画面の場合' do
      it 'ログインしている代表者トップページに移る' do
        sign_in_as(other_user)
        get edit_representative_user_path(rep, user)
        expect(response).to redirect_to root_path
      end
    end

    context '編集するユーザーがsenderの場合' do
      let(:user2) { create(:user, representative: rep) }

      it '本人であればアクセスに成功する' do
        sign_in_as(user)
        get edit_representative_user_path(rep, user)
        expect(response.status).to eq 200
        expect(response.body).to include user.name
      end

      it '本人以外の場合は代表者トップページに移る' do
        sign_in_as(user2)
        get edit_representative_user_path(rep, user)
        expect(response).to redirect_to representative_path(rep)
      end
    end

    context '編集するユーザーがreceiverの場合' do
      let(:user2) { create(:user, sender: 'receiver', representative: rep) }
      let(:user3) { create(:user, sender: 'receiver', representative: rep) }

      it 'ログインユーザーがsenderであれば成功する' do
        sign_in_as(user)
        get edit_representative_user_path(rep, user2)
        expect(response.status).to eq 200
        expect(response.body).to include user2.name
      end

      it 'ログインユーザーがsenderでなければ代表者トップページに移る' do
        sign_in_as(user3)
        get edit_representative_user_path(rep, user2)
        expect(response).to redirect_to root_path
      end
    end
  end

  # PATCH
  describe 'PATCH' do
    context 'ログインしてない場合' do
      it 'ログイン画面にリダイレクトする' do
        patch representative_user_path(rep, user), params: { user: { name: 'change' } }
        expect(response).to redirect_to login_path
      end
    end

    context 'ログインしている代表者アカウントと違う代表者のユーザー編集画面の場合' do
      let(:other_rep) { create(:representative) }
      let(:other_user) { create(:user, representative: other_rep) }

      it 'ログインしている代表者トップページに移る' do
        sign_in_as(other_user)
        patch representative_user_path(rep, user), params: { user: { name: 'change' } }
        expect(response).to redirect_to root_path
      end
    end

    context '編集するユーザーがsenderの場合' do
      let(:user2) { create(:user, representative: rep) }

      it '本人であれば更新に成功する' do
        sign_in_as(user)
        patch representative_user_path(rep, user), params: { user: { name: 'change' } }
        expect(response.status).to eq 302
        get representative_users_path(rep)
        expect(response.body).to include 'change'
      end

      it '本人以外の場合は代表者トップページに移る' do
        sign_in_as(user2)
        patch representative_user_path(rep, user), params: { user: { name: 'change' } }
        expect(response).to redirect_to representative_path(rep)
      end
    end

    context '編集するユーザーがreceiverの場合' do
      let(:receiver) { create(:user, name: 'before_receiver', sender: 'receiver', representative: rep) }
      let(:receiver2) { create(:user, sender: 'receiver', representative: rep) }

      it 'ログインユーザーがsenderであれば成功する' do
        sign_in_as(user)
        patch representative_user_path(rep, receiver), params: { user: { name: 'change' } }
        expect(response.status).to eq 302
        get representative_users_path(rep), params: { sender: '情報受信メンバー' }
        expect(response.body).to include 'change'
      end

      it 'ログインユーザーがsenderでなければ代表者トップページに移る' do
        sign_in_as(receiver2)
        patch representative_user_path(rep, receiver), params: { user: { name: 'change' } }
        expect(response).to redirect_to staff_representative_path(rep)
      end
    end

    context 'cityパラメータのみ送られた場合' do
      before do
        create(:city)
        create(:city, :nagoya)
        delete logout_path
        sign_in_as(user)
        patch representative_user_path(rep, user), params: { user: { city: '名古屋', city_id: 1856057 } }
      end

      it_behaves_like 'リクエストが成功する(302)'

      it 'cityパラメータが更新される' do
        expect(user.reload.city).to eq '名古屋'
        expect(user.reload.city_id).to eq 1856057
      end

      it 'ホームにリダイレクトされる' do
        expect(response).to redirect_to rep
      end
    end

    context '誤ったcityパラメータが送られた場合' do
      let(:receiver) { create(:user, name: 'receiver', sender: 'receiver', representative: rep) }
      before do
        create(:city)
        create(:city, :nagoya)
        sign_in_as(receiver)
        patch representative_user_path(rep, receiver), params: { user: { city: 'test', city_id: 187766 } }
      end

      it_behaves_like 'リクエストが成功する(302)'

      it 'cityパラメータが更新されない' do
        expect(user.reload.city).to eq '東京'
        expect(user.reload.city_id).to eq 1850147
      end

      it 'receiverの場合staffホームにリダイレクトされる' do
        expect(response).to redirect_to [:staff, rep]
      end
    end
  end

  describe 'DELETE' do
    let(:receiver) { create(:user, name: 'test', sender: 'receiver', representative: rep) }
    before do
      sign_in_as(user)
    end

    it '権限のあるユーザーだとアクセスできる' do
      delete representative_user_path(rep, receiver)
    end
  end
end
