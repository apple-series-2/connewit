require 'rails_helper'

RSpec.describe "Remember me", type: :request do
  let!(:user) { create(:user, representative: rep) }
  let(:rep) { create(:representative) }

  # 2つのバグのテスト1
  context 'ログアウト後別ウィンドウで再度ログアウトした時' do
    it 'ログイン中でのみログアウトすること' do
      sign_in_as(user)
      expect(response).to redirect_to representative_path(rep)

      # ログアウトする
      delete logout_path
      expect(response).to redirect_to root_path
      expect(session[:user_id]).to eq nil

      # 2番目のウィンドウでログアウトする
      delete logout_path
      expect(response).to redirect_to root_path
      expect(session[:user_id]).to eq nil
    end
  end

  # remember_meチェックボックスのテスト
  context 'remember_meにチェックした場合' do
    it 'クッキーに保存される' do
      remember_sign_in_as(user)
      expect(response.cookies['remember_token']).not_to eq nil
    end
  end

  context 'remember_meチェックでログイン後ログアウトすると' do
    it 'クッキーが削除されている' do
      remember_sign_in_as(user)
      delete logout_path
      sign_in_as(user)
      expect(response.cookies['remember_token']).to eq nil
    end
  end
end
