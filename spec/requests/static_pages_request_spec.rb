require 'rails_helper'

describe StaticPagesController, type: :request do
  shared_examples_for 'リクエストが成功する' do
    it { expect(response.status).to eq(200) }
  end

  describe "GET /home" do
    before do
      get root_path
    end

    it_behaves_like 'リクエストが成功する'
  end
end
