require 'rails_helper'

RSpec.describe "sessions", type: :request do
  subject { response.body }

  let!(:user) { create(:user, representative: rep) }
  let(:rep) { create(:representative) }
  let!(:user2) { create(:user, representative: rep2) }
  let(:rep2) { create(:representative, :not_activate) }

  shared_examples_for 'リクエストが成功する(200)' do
    it { expect(response.status).to eq(200) }
  end

  shared_examples_for 'リクエストが成功する(302)' do
    it { expect(response.status).to eq(302) }
  end

  # GET /signin
  describe 'GET /signin' do
    before { get login_path }

    it_behaves_like 'リクエストが成功する(200)'

    it '正しいページが表示されている' do
      is_expected.to include 'ユーザーログイン'
    end
  end

  # POST /signin
  describe 'POST /signin' do
    context 'パラメーターが正常な場合' do
      before { sign_in_as(user) }

      it_behaves_like 'リクエストが成功する(302)'

      it 'ログインする' do
        expect(session[:user_id]).to eq user.id
        expect(session[:representative_id]).to eq rep.id
      end

      it 'リダイレクトする' do
        expect(response).to redirect_to representative_path(rep)
      end
    end

    context 'ログインセッション以外のセッションが存在する場合' do
      it 'その他のセッションのみすべて削除される' do
        get representative_admin_signup_path(rep)
        expect(session[:params_action].present?).to eq true

        post login_url, params: { email: user.email, password: user.password }
        expect(session[:user_id]).to eq user.id
        expect(session[:representative_id]).to eq rep.id
        expect(session[:params_action]).to eq nil
      end
    end

    context 'パラメーターが不正の場合' do
      before { post login_path, params: { email: user.email, password: 'failpassword' } }

      it_behaves_like 'リクエストが成功する(200)'

      it 'ログインしない' do
        expect(session[:user_id]).to eq nil
        expect(session[:representative_id]).to eq nil
      end
    end

    context '紐付いた代表者アカウントが有効化されてない場合' do
      before { post login_path, params: { email: user2.email, password: user2.password } }

      it 'ログインしない' do
        expect(session[:user_id]).to eq nil
        expect(session[:representative_id]).to eq nil
      end

      it 'リダイレクトされる' do
        expect(response).to redirect_to root_path
      end
    end
  end

  # DELETE /signout
  describe 'DELETE /signout' do
    before do
      remember_sign_in_as(user)
      delete logout_url
    end

    it_behaves_like 'リクエストが成功する(302)'

    it 'セッションとクッキーが削除されている' do
      expect(session[:user_id]).to eq nil
      expect(cookies.signed[:user_id]).to eq ""
      expect(cookies.signed[:remember_token]).to eq ""
    end

    it 'リダイレクトする' do
      expect(response).to redirect_to root_path
    end
  end

  # GET /representative_login
  describe 'GET /representative_login' do
    before { get representative_login_path }

    it_behaves_like 'リクエストが成功する(200)'

    it '正しいページが表示されている' do
      is_expected.to include '代表者アカウント認証'
    end
  end

  # POST /representative_login
  describe 'POST /representative_login' do
    context 'パラメーターが正しい場合' do
      before { post representative_login_path, params: { email: rep.email, password: rep.password } }

      it_behaves_like 'リクエストが成功する(302)'

      it 'セッションにidが登録されている' do
        expect(session[:representative_id]).to eq rep.id
      end

      it 'リダイレクトする' do
        expect(response).to redirect_to new_representative_user_path(rep)
      end
    end

    context 'session[:params_action]が存在する場合' do
      it '認証後管理者ユーザー登録画面にリダイレクトされる' do
        get representative_admin_signup_path(rep)
        expect(session[:params_action]).to eq "new_admin_user"

        post representative_login_path, params: { email: rep.email, password: rep.password }
        expect(response).to redirect_to representative_admin_signup_path(rep)
      end
    end

    context 'パラメーターが誤っている場合' do
      before { post representative_login_path, params: { email: 'fail@test.com', password: rep.password } }

      it_behaves_like 'リクエストが成功する(200)'

      it 'セッションがnilである' do
        expect(session[:representative_id]).to eq nil
      end
    end

    context 'アカウントが有効化されてない場合' do
      before { post representative_login_path, params: { email: rep2.email, password: rep2.password } }

      it 'セッションがnilである' do
        expect(session[:representative_id]).to eq nil
      end

      it 'リダイレクトされる' do
        expect(response).to redirect_to root_path
      end
    end
  end
end
