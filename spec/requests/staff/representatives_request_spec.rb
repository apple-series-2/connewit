require 'rails_helper'

RSpec.describe "Staff::Representatives", type: :request do
  shared_examples_for 'リクエストが成功する(200)' do
    it { expect(response.status).to eq(200) }
  end

  shared_examples_for 'リクエストが成功する(302)' do
    it { expect(response.status).to eq(302) }
  end

  let!(:user) { create(:user, representative: rep) }
  let(:rep) { create(:representative) }
  let(:receiver) { create(:user, sender: 'receiver', representative: rep) }
  let(:rep2) { create(:representative) }

  # GET /show
  describe 'GET /show' do
    context 'リクエストが正常な場合' do
      before do
        sign_in_as(receiver)
        get staff_representative_path(rep)
      end

      it_behaves_like 'リクエストが成功する(200)'

      it 'ユーザー名が表示されている' do
        expect(response.body).to include receiver.name
      end
    end

    context 'セッションが正しくない場合' do
      it '別画面へリダイレクトされる' do
        # ログインしてなかった時
        get staff_representative_path(rep)
        expect(response).to redirect_to login_path(path: staff_representative_path(rep))
        # 関係ないアカウントへアクセスした時
        sign_in_as(receiver)
        expect(session[:representative_id]).to eq receiver.representative.id
        get staff_representative_path(rep2)
        expect(response).to redirect_to root_path
        # repのみ認証しユーザーログインはまだの時
        delete logout_path
        certification_as(rep)
        get staff_representative_path(rep)
        expect(response).to redirect_to login_path(path: staff_representative_path(rep))
      end
    end

    context 'ajax' do
      let!(:today_article) do
        create(:article, title: 'today-a', start_date: Date.today,
                         end_date: Date.tomorrow, user: user, representative: rep)
      end
      let!(:end_article) do
        create(:article, title: 'end-a', start_date: Date.today.prev_day(6),
                         end_date: Date.tomorrow, user: user, representative: rep)
      end
      let!(:start_article) do
        create(:article, title: 'start-a', start_date: Date.tomorrow,
                         end_date: Date.today.next_day(6), user: user, representative: rep)
      end

      before { sign_in_as(receiver) }

      context 'params type: todayの場合' do
        before { get staff_representative_path(rep), xhr: true, params: { type: 'today' } }

        it_behaves_like 'リクエストが成功する(200)'

        it '正しいレスポンスが帰ってくる' do
          expect(response.body).to include today_article.title, user.name
        end
      end

      context 'params type: end_planの場合' do
        before { get staff_representative_path(rep), xhr: true, params: { type: 'end_plan' } }

        it_behaves_like 'リクエストが成功する(200)'

        it '正しいレスポンスが帰ってくる' do
          expect(response.body).to include end_article.title, user.name
        end
      end

      context 'params type: start_planの場合' do
        before { get staff_representative_path(rep), xhr: true, params: { type: 'start_plan' } }

        it_behaves_like 'リクエストが成功する(200)'

        it '正しいレスポンスが帰ってくる' do
          expect(response.body).to include start_article.title, user.name
        end
      end
    end
  end
end
