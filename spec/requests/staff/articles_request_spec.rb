require 'rails_helper'

RSpec.describe "Sraff/Articles", type: :request do
  let!(:user) { create(:user, email: 'example@example.com', representative: rep) }
  let!(:receiver) { create(:user, sender: 'receiver', representative: rep) }
  let(:rep) { create(:representative, email: 'example@example.com') }

  shared_examples_for 'リクエストが成功する(200)' do
    it { expect(response.status).to eq(200) }
  end

  shared_examples_for 'リクエストが成功する(302)' do
    it { expect(response.status).to eq(302) }
  end

  describe 'GET /index' do
    require 'nokogiri'

    let(:rep2) { create(:representative) }
    let(:user2) { create(:user, representative: rep2) }
    let(:article2) { create(:article, user: user2, representative: rep2) }
    let!(:info) { create(:article, title: 'info-test-example', created_at: Date.yesterday, user: user, representative: rep) }
    let!(:information) { create_list(:article, 2, user: user, representative: rep) }
    let!(:plan) do
      create(:article, title: 'plans-test-example', created_at: Date.yesterday,
                       type: 'plans', user: receiver, representative: rep)
    end
    let!(:plans) { create_list(:article, 1, type: 'plans', user: receiver, representative: rep) }
    let(:html) { Nokogiri::HTML(response.body) }
    let(:article_container_doc) { html.css('.article-container') }

    before do
      sign_in_as(receiver)
      get staff_representative_articles_path(rep)
    end

    it_behaves_like 'リクエストが成功する(200)'

    it '正しいページが表示されている' do
      expect(response.body).to include 'info-test-example'
      expect(article_container_doc.size).to eq 3
    end

    it '他の代表者に紐づく記事は表示されない' do
      expect(response.body).not_to include article2.title
    end

    it 'type:plansを渡すと予定一覧が表示される' do
      get staff_representative_articles_path(rep), params: { type: 'スタッフの予定' }
      expect(response.body).to include 'plans-test-example'
      expect(article_container_doc.size).to eq 2
    end

    it 'keywordを渡すとtitleとcontent内で前後一致した情報が表示される' do
      get staff_representative_articles_path(rep), params: { keyword: 'fo-test', type: '連絡情報' }
      expect(response.body).to include 'info-test-example'
      expect(article_container_doc.size).to eq 1
      get staff_representative_articles_path(rep), params: { keyword: 'ns-test', type: 'スタッフの予定' }
      expect(response.body).to include 'plans-test-example'
      expect(article_container_doc.size).to eq 1
    end

    it 'orderを渡すと並び替えされる' do
      get staff_representative_articles_path(rep), params: { order: 'created_at ASC' }
      expect(article_container_doc.first.to_s).to include 'info-test-example'
    end

    it 'typeとorderを渡すと渡されたtypeのarticleで並び替えされる' do
      get staff_representative_articles_path(rep), params: { type: 'スタッフの予定', order: 'created_at ASC' }
      expect(article_container_doc.first.to_s).to include 'plans-test-example'
    end
  end

  describe 'GET /show' do
    let!(:info) { create(:article, user: user, representative: rep) }

    before do
      sign_in_as(receiver)
      get staff_representative_article_path(rep, info)
    end

    it_behaves_like 'リクエストが成功する(200)'

    it '正しい記事情報が渡されている' do
      expect(response.body).to include info.title
    end
  end

  describe 'GET /edit' do
    let!(:info) { create(:article, user: user, representative: rep) }
    let!(:plans) { create(:article, :plans, user: receiver, representative: rep) }

    before { sign_in_as(receiver) }

    it 'informationのeditにはアクセスできない' do
      get edit_staff_representative_article_path(rep, info)
      expect(response.status).to eq 302
      expect(response).to redirect_to [:staff, rep]
    end

    it 'plansのeditにはarticleの情報を持ってアクセスできる' do
      get edit_staff_representative_article_path(rep, plans)
      expect(response.status).to eq 200
      expect(response.body).to include plans.title
    end
  end

  describe 'GET /new' do
    context 'ログイン済みの場合' do
      before do
        sign_in_as(receiver)
        get new_staff_representative_article_path(rep)
      end

      it_behaves_like 'リクエストが成功する(200)'

      it '正しい画面が表示されている' do
        expect(response.body).to include '予定の登録'
      end
    end

    context '未ログインの場合' do
      before { get new_staff_representative_article_path(rep) }

      it_behaves_like 'リクエストが成功する(302)'

      it 'リダイレクトする' do
        expect(response).to redirect_to login_path(path: new_staff_representative_article_path(rep))
      end
    end
  end

  describe 'POST' do
    let(:plan) { create(:article, type: 'plans', parent_tag: nil, child_tag: nil, user: receiver, representative: rep) }
    before { sign_in_as(receiver) }

    context 'リクエストが正しい場合' do
      before do
        post staff_representative_articles_path(rep),
             params: {
               article: attributes_for(:article, type: 'plans', parent_tag: nil, child_tag: nil,
                                                 user_id: receiver.id, representative_id: rep.id),
             }
      end

      it_behaves_like 'リクエストが成功する(302)'

      it '正しく登録されincompleteも登録される' do
        expect(Article.all.size).to eq 1
        expect(Article.last.plan_days.present?).to eq true
        expect(Incomplete.last.article_id).to eq Article.last.id
        expect(Incomplete.last.user_id).to eq receiver.id
      end
    end

    context 'リクエストが正しくない場合' do
      before do
        post staff_representative_articles_path(rep),
             params: {
               article: attributes_for(:article, :invalid, type: 'plans',
                                                           user_id: user.id, representative_id: rep.id),
             }
      end

      it_behaves_like 'リクエストが成功する(200)'

      it 'informationは投稿できない。' do
        post staff_representative_articles_path(rep),
             params: { article: attributes_for(:article, user_id: user.id, representative_id: rep.id) }
        expect(response.status).to eq(302)
        expect(Article.all.size).to eq 0
      end
    end
  end

  describe 'PATCH' do
    let!(:plan) { create(:article, type: 'plans', parent_tag: nil, child_tag: nil, user: receiver, representative: rep) }

    before { sign_in_as(receiver) }

    context 'パラメータが正しい場合' do
      before do
        patch staff_representative_article_path(rep, plan),
              params: { article: { title: 'タイトル変更', complete_function: 1 } }
      end

      it 'リクエストが成功し変更が反映される' do
        expect(response.status).to eq(302)
        get staff_representative_article_path(rep, plan)
        expect(response.body).to include 'タイトル変更'
      end

      it '登録後リダイレクトする' do
        expect(response).to redirect_to staff_representative_article_path(rep, plan)
      end
    end

    context 'パラメータが正しくない場合' do
      before { patch staff_representative_article_path(rep, plan), params: { article: { title: '' } } }

      it 'リクエストが成功しても変更は反映されない' do
        expect(response.status).to eq(200)
        get staff_representative_article_path(rep, plan)
        expect(response.body).to include 'testTitle'
      end
    end

    context 'complete_functionがfalseの場合' do
      before { patch staff_representative_article_path(rep, plan), params: { article: { title: '', complete_function: false } } }

      it 'リクエストが成功しても変更は反映されない' do
        expect(response.status).to eq(200)
        get staff_representative_article_path(rep, plan)
        expect(response.body).to include 'testTitle'
      end
    end
  end
end
