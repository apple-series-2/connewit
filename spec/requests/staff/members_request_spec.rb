require 'rails_helper'

RSpec.describe 'Staff::Members', type: :request do
  let(:rep) { create(:representative) }
  let!(:receiver) { create(:user, sender: 'receiver', representative: rep) }

  shared_examples_for 'アクセスに成功する' do
    it { expect(response).to have_http_status(:success) }
  end

  shared_examples_for 'アクセスに成功し302ステータスが返る' do
    it { expect(response).to have_http_status(302) }
  end

  before { sign_in_as(receiver) }

  describe 'GET /index' do
    let(:html) { Nokogiri::HTML(response.body) }
    let(:member_partial_doc) { html.css('.member-partial_container') }
    let!(:receiver2) { create(:user, sender: 'receiver', representative: rep) }
    let!(:members) { create_list(:member, 2, user: receiver) }
    let!(:other_member) { create(:member, name: 'other_member', user: receiver2) }

    it 'アクセスに成功する' do
      get staff_representative_members_path(rep)
      expect(response).to have_http_status(:success)
    end

    it 'ログイン中のユーザーに紐づくメンバーのみ登録されている' do
      get staff_representative_members_path(rep)
      expect(member_partial_doc.size).to eq 2
      expect(response.body).not_to include 'other_member'
    end
  end

  describe 'GET /new' do
    before { get new_staff_representative_member_path(rep) }

    it_behaves_like 'アクセスに成功する'

    it 'ユーザー情報が保持された正しい画面である' do
      expect(response.body).to include receiver.name
    end
  end

  describe 'POST' do
    context 'パラメータが正しい時' do
      before do
        post staff_representative_members_path(rep), params: { members: [
          { name: 'member1', user_id: receiver.id },
          { name: 'member2', user_id: receiver.id },
        ] }
      end

      it_behaves_like 'アクセスに成功し302ステータスが返る'

      it '登録されリダイレクトする' do
        expect(Member.all.size).to eq 2
        Member.all.each_with_index do |member, i|
          expect(member.name).to eq "member#{i + 1}"
          expect(member.user_id).to eq receiver.id
        end
        expect(response).to redirect_to staff_representative_members_path(rep)
      end
    end

    context 'いずれかのパラメータが正しくない時' do
      before do
        post staff_representative_members_path(rep), params: { members: [
          { name: 'member1', user_id: receiver.id },
          { name: 'member2', user_id: 2 },
        ] }
      end

      it_behaves_like 'アクセスに成功する'

      it '一つも登録されない' do
        expect(Member.all.size).to eq 0
      end
    end

    context '名前が空欄のパラメータが存在した場合' do
      before do
        post staff_representative_members_path(rep), params: { members: [
          { name: 'member1', user_id: receiver.id },
          { name: '', user_id: receiver.id },
        ] }
      end

      it_behaves_like 'アクセスに成功し302ステータスが返る'

      it '空欄でないデータのみ登録する' do
        expect(Member.all.size).to eq 1
      end
    end
  end

  describe 'PATCH' do
    let!(:member) { create(:member, name: 'before', user: receiver) }

    context 'パラメータが正しい時' do
      before { patch staff_representative_member_path(rep, member), params: { member: { name: 'after' } } }

      it_behaves_like 'アクセスに成功する'

      it '更新に成功する' do
        expect(member.reload.name).to eq 'after'
      end
    end
    # 正しくない場合のテストは、jsが行っている例外処理をrspecが捉えられないため見送り
  end

  describe 'DESTROY' do
    let!(:member) { create(:member, name: 'before', user: receiver) }

    before { delete staff_representative_member_path(rep, member) }

    it_behaves_like 'アクセスに成功し302ステータスが返る'

    it 'データが削除されリダイレクトする' do
      expect(Member.all.size).to eq 0
      expect(response).to redirect_to staff_representative_members_path(rep)
    end
  end
end
