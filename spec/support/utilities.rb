def sign_in_as(user)
  post login_url, params: { email: user.email, password: user.password }
end

def certification_as(rep)
  post representative_login_url, params: { email: rep.email, password: rep.password }
end

def feature_sign_in(user)
  visit login_url
  fill_in 'メールアドレス', with: user.email
  fill_in 'パスワード', with: user.password
  click_button 'ログイン'
end

def feature_certification_as(rep)
  visit representative_login_url
  fill_in 'メールアドレス', with: rep.email
  fill_in 'パスワード', with: rep.password
  click_button '認証'
end

def remember_sign_in_as(user)
  post login_url, params: { email: user.email, password: user.password, remember_me: '1' }
end
