require "rails_helper"

RSpec.describe UserMailer, type: :mailer do
  let!(:rep) { create(:representative, :not_activate, email: 'example@example.com') }
  let!(:user) { create(:user, email: 'example@example.com', representative: rep) }

  before { user.create_reset_digest }

  describe "account_activation" do
    let(:mail) { UserMailer.account_activation(rep) }

    it "メールヘッダー" do
      expect(mail.subject).to eq("【ConneWat】 会員仮登録完了のお知らせ")
      expect(mail.to).to eq(["example@example.com"])
      expect(mail.from).to eq(["noreply@example.com"])
    end
  end

  describe "password_reset" do
    let(:mail) { UserMailer.password_reset(user) }

    it "メールヘッダー" do
      expect(mail.subject).to eq("【ConneWat】 パスワードの再設定")
      expect(mail.to).to eq(["example@example.com"])
      expect(mail.from).to eq(["noreply@example.com"])
    end
  end
end
