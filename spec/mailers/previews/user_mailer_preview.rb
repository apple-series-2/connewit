# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/account_activation
  def account_activation
    rep = Representative.first
    rep.activation_token = Representative.new_token
    UserMailer.account_activation(rep)
  end

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/password_reset
  def password_reset
    user = User.first
    user.reset_token = User.new_token
    UserMailer.password_reset(user)
  end

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/account_destroy
  def destroy_account
    rep = Representative.first
    user = rep.admin_users.first
    rep.destroy_token = Representative.new_token
    UserMailer.destroy_account(user)
  end
end
