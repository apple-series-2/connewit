Rails.application.routes.draw do
  root 'static_pages#home'
  get '/login',     to: 'sessions#new'
  post '/login',    to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  get '/representative_login',  to: 'sessions#representative_new'
  post '/representative_login', to: 'sessions#representative_create'
  get '/sender_login', to: 'sample_login#sender_login'
  get '/receiver_login', to: 'sample_login#receiver_login'
  get '/functions', to: 'static_pages#functions'

  scope module: 'admin' do
    get '/tag_suggest', to: 'articles#tag_suggest'
    resources :representatives, only: [:new, :create, :show, :destroy] do
      get '/admin_signup', to: 'representatives#new_admin_user' 
      get '/event', to: 'representatives#event'
      resources :destroy_accounts,   only: [:new, :create, :edit, :destroy]
      resources :articles do
        resources :images, only: :destroy
        resources :incompletes, only: [:index, :update, :destroy]
        delete 'all_incompletes', to: 'incompletes#all_destroy'
      end
      resources :users, expect: :show do
        get '/complete', to: 'users#complete'
        get '/add_admin', to: 'users#add_admin'
        get '/delete_admin', to: 'users#delete_admin'
      end
    end
  end

  namespace :staff do
    post '/login', to: 'sessions#create'
    delete '/logout', to: 'sessions#destroy'
    resources :representatives, only: :show do
      get '/event', to: 'representatives#event'
      resources :articles do
        resources :incompletes, only: [:update, :destroy]
      end
      resources :members, except: [:show, :edit]
    end
  end

  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
end
